﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using LangLojanEngine.Utilities;

namespace ConsoleApplicationExtensions
{
    // Created by Shahram Pourmakhdomi
    // Greatly improved by Martin Lindstedt
    public abstract class ConsoleExtension
    {
        protected struct UIntInputRange
        {
            private const uint _minValue = uint.MinValue;
            private const uint _maxValue = uint.MaxValue - 1;
            private const uint _abortValue = uint.MaxValue;
            public uint MinValue { get; }
            public uint MaxValue { get; }
            public uint AbortValue { get; }

            public UIntInputRange(uint minValue = _minValue, uint maxValue = _maxValue, uint abortValue = _abortValue)
            {
                if (minValue > maxValue || (minValue > abortValue && abortValue > maxValue))
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                    AbortValue = _abortValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                    AbortValue = abortValue;
                }
            }
        }
        protected struct IntInputRange
        {
            private const int _minValue = int.MinValue;
            private const int _maxValue = int.MaxValue - 1;
            private const int _abortValue = int.MaxValue;
            public int MinValue { get; }
            public int MaxValue { get; }
            public int AbortValue { get; }

            public IntInputRange(int minValue = _minValue, int maxValue = _maxValue, int abortValue = _abortValue)
            {
                if (minValue > maxValue || (minValue > abortValue && abortValue > maxValue))
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                    AbortValue = _abortValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                    AbortValue = abortValue;
                }
            }
        }
        protected struct DoubleInputRange
        {
            private const double _minValue = double.MinValue;
            private const double _maxValue = double.MaxValue - 1;
            private const double _abortValue = double.MaxValue;
            public double MinValue { get; }
            public double MaxValue { get; }
            public double AbortValue { get; }

            public DoubleInputRange(double minValue = _minValue, double maxValue = _maxValue, double abortValue = _abortValue)
            {
                if (minValue > maxValue || (minValue > abortValue && abortValue > maxValue))
                {
                    MinValue = _minValue;
                    MaxValue = _maxValue;
                    AbortValue = _abortValue;
                }
                else
                {
                    MinValue = minValue;
                    MaxValue = maxValue;
                    AbortValue = abortValue;
                }
            }
        }
        protected struct DateTimeInputRange
        {
            public DateTime MinDateTime { get; }
            public DateTime MaxDateTime { get; }
            public string AbortValue { get; }

            public DateTimeInputRange(DateTime minDateTime, DateTime maxDateTime, string abortValue = "0")
            {
                if (minDateTime.Date > maxDateTime.Date)
                {
                    MinDateTime = DateTime.MinValue;
                    MaxDateTime = DateTime.MaxValue;
                }
                else
                {
                    MinDateTime = minDateTime;
                    MaxDateTime = maxDateTime;
                }
                AbortValue = abortValue;
            }
        }
        protected struct OptionsStringArray
        {
            public string[] Options { get; }
            public bool OptionsStartAtZero { get; }
            public int AbortValue => -1;

            public OptionsStringArray(string[] options, bool optionsStartAtZero = false)
            {
                if (options == null)
                    throw new ArgumentNullException($"{nameof(options)} cannot be null.");
                if (options.Length == 0)
                    throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");

                Options = options;
                OptionsStartAtZero = optionsStartAtZero;
            }
        }
        protected struct OptionsDictionary
        {
            public Dictionary<ConsoleKey, string> Options { get; }
            public bool NumPadKeysAreUnique { get; }
            public ConsoleKey AbortKey { get; }

            public OptionsDictionary(Dictionary<ConsoleKey, string> options, bool numPadKeysAreUnique = false, ConsoleKey abortKey = ConsoleKey.Escape)
            {
                if (options == null)
                    throw new ArgumentNullException($"{nameof(options)} cannot be null.");
                if (options.Count == 0)
                    throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");
                if (options.ContainsKey(abortKey))
                    throw new ArgumentException($"{nameof(options)} cannot contain the abort key.");

                Options = options;
                NumPadKeysAreUnique = numPadKeysAreUnique;
                AbortKey = abortKey;
            }
        }
        
        private const int DefaultMaxInputAttempts = 5;

        private bool _keepRunning;
        private readonly Dictionary<ConsoleKey, Action> _commandMapper;
        private readonly Dictionary<ConsoleKey, string> _commandLabelMapper;

        protected ConsoleExtension()
        {
            Console.ForegroundColor = ConsoleColor.White;
            _commandMapper = new Dictionary<ConsoleKey, Action>();
            _commandLabelMapper = new Dictionary<ConsoleKey, string>();
            Initialize();
            RunLoop();
        }

        protected abstract void Initialize();
        protected void RunLoop()
        {
            _keepRunning = true;
            do
            {
                PrintMenu();
                var command = GetCommand();
                ExecuteCommand(command);
            } while (_keepRunning);
        }
        protected void EndLoop()
        {
            _keepRunning = false;
        }
        protected ConsoleKey GetCommand()
        {
            var command = Console.ReadKey().Key;
            Console.WriteLine();
            return command;
        }

        protected void ExecuteCommand(ConsoleKey command)
        {
            if (_commandMapper.ContainsKey(command))
            {
                var action = _commandMapper[command];
                Console.Clear();
                action();
            }
            else
            {
                Console.WriteLine("Unknown command!");
            }
        }
        protected void AddCommand(ConsoleKey command, string commandLabel, Action commandAction)
        {
            _commandLabelMapper[command] = commandLabel;
            _commandMapper[command] = commandAction;
        }

        private void PrintMenu()
        {
            Console.Clear();
            PrintMenuBorder();
            foreach (var command in _commandLabelMapper.Keys)
            {
                var label = _commandLabelMapper[command];
                Console.WriteLine($"[{Convert.ToChar(command)}] {label}");
            }
            PrintMenuBorder();
            Console.Write("Enter one of the above commands: ");
        }

        private void PrintMenuBorder()
        {
            int borderLength = 0;
            foreach (var command in _commandLabelMapper.Keys)
            {
                borderLength = ((_commandLabelMapper[command].Length + 4) > borderLength) ? _commandLabelMapper[command].Length + 4 : borderLength;
            }
            for (int i = 0; i < borderLength; i++)
            {
                Console.Write("=");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Returns abortValue if maximum input attempts is reached.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        protected void AskFor(out uint answer, string prompt, UIntInputRange inputRange = new UIntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinValue == inputRange.MaxValue ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                Console.Write($"{prompt}{interval}, {inputRange.AbortValue} to abort: ");
                string line = Console.ReadLine();
                if (uint.TryParse(line, out answer))
                {
                    if (answer == inputRange.AbortValue)
                        return;
                    if (inputRange.MinValue <= answer || answer <= inputRange.MaxValue)
                        return;
                    Console.WriteLine($"The value is outside the the set interval{(string.IsNullOrEmpty(interval) ? "" : interval)}.");
                }
                else
                    Console.WriteLine("Please enter a integer.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            answer = inputRange.AbortValue;
        }

        /// <summary>
        /// Returns abortValue if maximum input attempts is reached.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        protected void AskFor(out int answer, string prompt, IntInputRange inputRange = new IntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = Math.Abs(inputRange.MinValue) == inputRange.MaxValue ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                Console.Write($"{prompt}{interval}, {inputRange.AbortValue} to abort: ");
                string line = Console.ReadLine();
                if (int.TryParse(line, out answer))
                {
                    if (answer == inputRange.AbortValue)
                        return;
                    if (inputRange.MinValue <= answer && answer <= inputRange.MaxValue)
                        return;
                    Console.WriteLine($"The value is outside the the set interval{interval}.");
                }
                else
                    Console.WriteLine("Please enter a integer.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            answer = inputRange.AbortValue;
        }

        protected void AskFor(out int answer, string prompt, OptionsStringArray options, 
                              IntInputRange inputRange = new IntInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            string option = "[{0}]";
            if (options.Options.Length < 10)
                option += " ";
            else if (options.Options.Length < 100)
                option += "  ";
            else
                option += "   ";
            option += "{1}";
            int startValue = options.OptionsStartAtZero ? 0 : 1;
            for (int i = 0; i < options.Options.Length; i++)
            {
                Console.WriteLine(String.Format(option, i+ startValue, options.Options[i]));
            }
            AskFor(out answer, prompt, new IntInputRange(inputRange.MinValue, inputRange.MaxValue, options.AbortValue), maxInputAttempts);
        }

        /// <summary>
        /// Returns 0 if maximum abortValue attempts is reached.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        protected void AskFor(out double answer, string prompt, DoubleInputRange inputRange = new DoubleInputRange(), int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinValue == inputRange.MaxValue ? "" : $" ({inputRange.MinValue} to {inputRange.MaxValue})";

            while (counter < maxInputAttempts)
            {
                Console.Write($"{prompt}{interval}, {inputRange.AbortValue} to abort: ");
                string line = Console.ReadLine();
                if (double.TryParse(line, out answer))
                {
                    if (answer == inputRange.AbortValue)
                        return;
                    if (inputRange.MinValue <= answer || answer <= inputRange.MaxValue)
                        return;
                    Console.WriteLine($"The value is outside the the set interval{interval}.");
                }
                else
                    Console.WriteLine("Please enter a decimal value.");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            answer = inputRange.AbortValue;
        }

        /// <summary>
        /// Returns DateTime.MinValue if maximum input attempts is reached.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        /// <param name="inputRange"></param>
        /// <param name="maxInputAttempts"></param>
        protected void AskFor(out DateTime answer, string prompt, DateTimeInputRange inputRange, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            string interval = inputRange.MinDateTime.Date == inputRange.MaxDateTime.Date ? "" : $" ({inputRange.MinDateTime.ToShortDateString()} to {inputRange.MaxDateTime.ToShortDateString()})";

            while (counter < maxInputAttempts)
            {
                Console.Write($"{prompt}{interval}, {inputRange.AbortValue} to abort: ");
                string line = Console.ReadLine();
                if (DateTime.TryParse(line, out answer))
                {
                    if (inputRange.MinDateTime.Date <= answer.Date || answer.Date <= inputRange.MaxDateTime.Date)
                        return;
                    Console.WriteLine($"The date is outside the the set interval{interval}.");
                }
                if (line.Length == 8)
                {
                    int tmp;
                    if (int.TryParse(line, out tmp))
                    {
                        line = line.Insert(6, "-");
                        line = line.Insert(4, "-");
                        answer = DateTime.Parse(line);
                        return;
                    }
                }
                if (line == inputRange.AbortValue)
                    return;

                Console.WriteLine("Please enter a date value (yyyy-mm-dd) or (yyyymmdd).");

                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            answer = DateTime.MinValue;
        }

        /// <summary>
        /// Returns a string.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        protected void AskFor(out string answer, string prompt)
        {
            Console.Write($"{prompt}: ");

            answer = Console.ReadLine();
        }

        /// <summary>
        /// Returns false if maximum input attempts is reached.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="prompt"></param>
        /// <param name="noAsDefaultAnswer">Default false</param>
        /// <param name="maxInputAttempts">Default 5</param>
        protected void AskFor(out bool answer, string prompt, bool noAsDefaultAnswer = false, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;

            while (counter < maxInputAttempts)
            {
                Console.Write($"{prompt} {(noAsDefaultAnswer ? "(y/N)" : "(Y/n)")}: ");
                ConsoleKey inputKey = Console.ReadKey().Key;
                Console.WriteLine();
                switch (inputKey)
                {
                    case ConsoleKey.Y:
                        answer = true;
                        return;
                    case ConsoleKey.N:
                        answer = false;
                        return;
                    case ConsoleKey.Enter:
                        answer = !noAsDefaultAnswer;
                        return;
                }
                counter++;
            }

            answer = false;
            PrintMaxInputAttemptsMessage(maxInputAttempts);
        }
        
        /// <summary>
        /// Returns a ConsoleKey.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="promt"></param>
        protected void AskFor(out ConsoleKey answer, string promt)
        {
            Console.Write($"{promt}: ");

            answer = Console.ReadKey().Key;
            Console.WriteLine();
        }

        /// <summary>
        /// Returns a ConoleKey.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="promt"></param>
        /// <param name="options"></param>
        /// <param name="maxInputAttempts"></param>
        protected void AskFor(out ConsoleKey answer, string promt, OptionsDictionary options, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;
            Console.WriteLine(promt);
            foreach (var kvp in options.Options)
            {
                Console.WriteLine($"[{GetSubstringOfConsoleKey(kvp.Key)}] {kvp.Value}");
            }
            Console.WriteLine($"[{GetSubstringOfConsoleKey(options.AbortKey)}] Abort");
            while (counter < maxInputAttempts)
            {
                Console.Write("Enter a key: ");
                answer = Console.ReadKey().Key;
                Console.WriteLine();
                if (!options.NumPadKeysAreUnique)
                    answer = answer.ToDigitKey();
                if (options.Options.ContainsKey(answer) || answer == options.AbortKey)
                    return;

                Console.WriteLine("Not a valid key.");
                counter++;
            }

            answer = options.AbortKey;
            PrintMaxInputAttemptsMessage(maxInputAttempts);
        }

        private string GetSubstringOfConsoleKey(ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            if (str.Length == 2 && str[0] == 'D')
                return str[1].ToString();

            return str;
        }

        protected Dictionary<ConsoleKey, string> CreateOptionsDictionary(ConsoleKey[] keys, string[] options)
        {
            if (keys == null)
                throw new ArgumentNullException($"{nameof(keys)} cannot be null.");
            if (keys.Length == 0)
                throw new ArgumentException($"{nameof(keys)} must contain at least 1 key.");
            if (options == null)
                throw new ArgumentNullException($"{nameof(options)} cannot be null.");
            if (options.Length == 0)
                throw new ArgumentException($"{nameof(options)} must contain at least 1 option.");
            if (keys.Length != options.Length)
                throw new ArgumentException($"{nameof(keys)} and {nameof(options)} must contain the same amount of objects.");
            if (keys.Length != keys.Distinct().Count())
                throw new ArgumentException("All keys must be unique.");

            Dictionary<ConsoleKey, string> optionsDictionary = new Dictionary<ConsoleKey, string>();
            for (int i = 0; i < keys.Length; i++)
            {
                optionsDictionary.Add(keys[i], options[i]);
            }

            return optionsDictionary;
        }

        protected ConsoleKey[] GetDigitKeys(int numberOfKeys, bool startAtZero = true)
        {
            if (numberOfKeys < 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfKeys)} cannot be negative.");
            if (numberOfKeys > 10)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfKeys)} is to large. Max is 10 digit keys.");
            if (numberOfKeys == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfKeys)} is to small. Min is 1 digit key.");

            List<ConsoleKey> keys = new List<ConsoleKey>();
            int startIndex = startAtZero ? 0 : 1;
            for (int i = startIndex; i < numberOfKeys + startIndex; i++)
            {
                try
                {
                    keys.Add(_digitKeys[i]);
                }
                catch
                {
                    keys.Add(_digitKeys[0]);
                }
            }

            return keys.ToArray();
        }

        protected ConsoleKey[] GetNumPadKeys(uint numberOfKeys, bool startAtZero = true)
        {
            if (numberOfKeys > 10)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfKeys)} is to large. Max is 10 NumPad keys.");
            if (numberOfKeys == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfKeys)} is to small. Min is 1 NumPad key.");

            List<ConsoleKey> keys = new List<ConsoleKey>();
            int startIndex = startAtZero ? 0 : 1;
            for (int i = startIndex; i < numberOfKeys; i++)
            {
                try
                {
                    keys.Add(_numPadKeys[i]);
                }
                catch
                {
                    keys.Add(_numPadKeys[0]);
                }
            }

            return keys.ToArray();
        }

        private readonly ConsoleKey[] _digitKeys =
        {
            ConsoleKey.D0, ConsoleKey.D1, ConsoleKey.D2, ConsoleKey.D3, ConsoleKey.D4,
            ConsoleKey.D5, ConsoleKey.D6, ConsoleKey.D7, ConsoleKey.D8, ConsoleKey.D9
        };

        private readonly ConsoleKey[] _numPadKeys =
        {
            ConsoleKey.NumPad0, ConsoleKey.NumPad1, ConsoleKey.NumPad2, ConsoleKey.NumPad3, ConsoleKey.NumPad4,
            ConsoleKey.NumPad5, ConsoleKey.NumPad6, ConsoleKey.NumPad7, ConsoleKey.NumPad8, ConsoleKey.NumPad9,
        };

        protected int NumberKeyToInt(ConsoleKey numberKey)
        {
            switch (numberKey)
            {
                case ConsoleKey.NumPad0:
                case ConsoleKey.D0:
                    return 0;
                case ConsoleKey.NumPad1:
                case ConsoleKey.D1:
                    return 1;
                case ConsoleKey.NumPad2:
                case ConsoleKey.D2:
                    return 2;
                case ConsoleKey.NumPad3:
                case ConsoleKey.D3:
                    return 3;
                case ConsoleKey.NumPad4:
                case ConsoleKey.D4:
                    return 4;
                case ConsoleKey.NumPad5:
                case ConsoleKey.D5:
                    return 5;
                case ConsoleKey.NumPad6:
                case ConsoleKey.D6:
                    return 6;
                case ConsoleKey.NumPad7:
                case ConsoleKey.D7:
                    return 7;
                case ConsoleKey.NumPad8:
                case ConsoleKey.D8:
                    return 8;
                case ConsoleKey.NumPad9:
                case ConsoleKey.D9:
                    return 9;
                default:
                    return -1;
            }
        }

        /// <summary>
        /// Returns false if maximum input attempts is reached.
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="noAsDefaultAnswer"></param>
        /// <param name="maxInputAttempts"></param>
        /// <returns></returns>
        protected bool Confirm(string prompt, bool noAsDefaultAnswer = false, int maxInputAttempts = DefaultMaxInputAttempts)
        {
            int counter = 0;

            while (counter < maxInputAttempts)
            {
                string promtEnding = noAsDefaultAnswer ? "(y/N)" : "(Y/n)";
                Console.Write($"{prompt} {promtEnding}:");
                ConsoleKey inputKey = Console.ReadKey().Key;
                Console.WriteLine();
                switch (inputKey)
                {
                    case ConsoleKey.Y:
                        return true;
                    case ConsoleKey.N:
                        return false;
                    case ConsoleKey.Enter:
                        return !noAsDefaultAnswer;
                }
                counter++;
            }

            PrintMaxInputAttemptsMessage(maxInputAttempts);
            return false;
        }

        /// <summary>
        /// Writes to console: reason (if not null), exception (if not null) and "Returning to main menu.". Option: Print with error colors
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="exception"></param>
        /// <param name="printErrorColors"></param>
        protected void PrintReturningToMainMenu(string reason = null, Exception exception = null, bool printErrorColors = true)
        {
            if (printErrorColors)
                PrintError(reason, exception, false);
            else
            {
                if (!string.IsNullOrWhiteSpace(reason))
                    Console.WriteLine(reason);
                if (exception != null)
                    Console.WriteLine(exception);
                Console.WriteLine();
            }
            Console.WriteLine("Returning to main menu.");
            Console.ReadKey(false);
        }

        protected void Print(string prompt,
                             bool waitForKeyPress = true,
                             ConsoleColor backgroundColor = ConsoleColor.Black,
                             ConsoleColor foregroundColor = ConsoleColor.White)
        {
            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(prompt);
            Console.ResetColor();
            if (waitForKeyPress)
                Console.ReadKey(false);
        }

        protected void PrintError(string message, Exception exception = null, bool waitForKeyPress = true)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
            if (exception != null)
            {
                Console.BackgroundColor = ConsoleColor.DarkYellow;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(exception);
            }
            Console.ResetColor();
            Console.WriteLine();
            if (waitForKeyPress)
                Console.ReadKey(false);
        }

        private void PrintMaxInputAttemptsMessage(int maxInputAttempts)
        {
            Console.WriteLine($"Maximum input attempts ({maxInputAttempts}) reached.");
        }
    }
}
