﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Xml.Linq;
using ConsoleApplicationExtensions;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Utilities;

namespace TestApp
{
    internal class Program : ConsoleExtension
    {
        private class CardCollectionFileNamePair
        {
            public CardCollection CardCollection { get; set; }
            public string FileName { get; set; }

            public void SetValues(CardCollection cardCollection, string fileName)
            {
                CardCollection = cardCollection;
                FileName = fileName;
            }
        }

        private class TableFileNamePair
        {
            public Table Table { get; set; }
            public string FileName { get; set; }

            public void SetValues(Table table, string fileName)
            {
                Table = table;
                FileName = fileName;
            }
        }

        private enum ItemType
        {
            Debug,
            Played,
            ContainingBugs
        }

        private enum Item
        {
            CardCollection,
            Table
        }

        private ConsoleColor[] _matchingCardsColors => new[]
        {
            ConsoleColor.DarkGreen, ConsoleColor.DarkRed, ConsoleColor.DarkYellow, ConsoleColor.DarkBlue,
            ConsoleColor.DarkCyan, ConsoleColor.DarkMagenta, ConsoleColor.DarkGray
        };

        private ConsoleColor[] _movableCardColors => new[]
        {
            ConsoleColor.Green, ConsoleColor.Red, ConsoleColor.Yellow, ConsoleColor.Blue,
            ConsoleColor.Cyan, ConsoleColor.Magenta, ConsoleColor.Gray
        };

        private CardCollectionFileNamePair LoadedCardCollectionFileNamePair
            => _ccfnp ?? (_ccfnp = new CardCollectionFileNamePair());

        private CardCollectionFileNamePair _ccfnp;

        private ReadOnlyCardCollection LoadedCardCollection => LoadedCardCollectionFileNamePair.CardCollection == null
            ? null
            : new ReadOnlyCardCollection(LoadedCardCollectionFileNamePair.CardCollection);

        private string LoadedCardCollectionFileName => LoadedCardCollectionFileNamePair.FileName;

        private TableFileNamePair LoadedTableFileNamePair => _tfnp ?? (_tfnp = new TableFileNamePair());
        private TableFileNamePair _tfnp;
        private Table LoadedTable => LoadedTableFileNamePair.Table;
        private string LoadedTableFileName => LoadedTableFileNamePair.FileName;
        private string UnsavedItem => nameof(UnsavedItem);
        private string MainDirectory => "Resources";

        protected override void Initialize()
        {
            string message;
            Exception exception;
            if (!TryFindMainDirectories(out message, out exception))
                PrintError(message, exception);

            AddCommand(ConsoleKey.L, "Load file", LoadFile);
            AddCommand(ConsoleKey.V, "View loaded item", ViewLoadedItem);
            AddCommand(ConsoleKey.U, "Unload item", UnloadItem);
            AddCommand(ConsoleKey.S, "Start new game", StartNewGame);
            AddCommand(ConsoleKey.Q, "Quit", Quit);
        }

        private bool TryFindMainDirectories(out string message, out Exception exception)
        {
            message = "Could not create one or more of the following directories:";
            foreach (Item item in Enum.GetValues(typeof(Item)))
            {
                foreach (ItemType itemType in Enum.GetValues(typeof(ItemType)))
                {
                    string path;
                    if (TryGetPath.ToDirectory(new[] { MainDirectory, item.ToString(), itemType.ToString() }, true,
                                               out path, out exception))
                        continue;

                    message += $"\n{MainDirectory}, {item}, {itemType}";
                    return false;
                }
            }

            message = null;
            exception = null;
            return true;
        }

        private void PrintSideBySide(IEnumerable<Card> cards1, IEnumerable<Card> cards2)
        {
            if (cards1 == null)
                throw new ArgumentNullException($"{nameof(cards1)} cannot be null.");
            if (cards2 == null)
                throw new ArgumentNullException($"{nameof(cards2)} cannot be null.");
            var eCards1 = cards1 as Card[] ?? cards1.ToArray();
            var eCards2 = cards2 as Card[] ?? cards2.ToArray();

            int numberOfCards = eCards1.Length > eCards2.Length ? eCards1.Length : eCards2.Length;
            int longestStringCount = eCards1.Length > eCards2.Length ? eCards1.ToStringArray().LongestStringCount() : eCards2.ToStringArray().LongestStringCount();

            for (int i = 0; i < numberOfCards; i++)
            {
                try
                {
                    Console.Write($"{eCards1[i].ToString().Replace(", ", "")}");
                    for (int j = 0; j < longestStringCount - eCards1[i].ToString().Length; j++)
                    {
                        Console.Write(" ");
                    }
                }
                catch
                {
                    for (int j = 0; j < longestStringCount; j++)
                    {
                        Console.Write(" ");
                    }
                }
                try
                {
                    Console.WriteLine($"   {eCards2[i].ToString().Replace(", ", "")}");
                }
                catch
                {
                    Console.WriteLine("   ");
                }
            }
            Console.WriteLine();
        }

        private static void Main(string[] args)
        {
            new Program();
        }

        private void SaveToXml(out bool saveSuccessful, out string fileName)
        {
            string message;
            Exception exception;
            if (!TrySaveLoadedTableToXml(out fileName, out message, out exception))
            {
                saveSuccessful = false;
                fileName = UnsavedItem;
                PrintError(message, exception, false);
                if (!Confirm("Do you want to start the game anyway?", true))
                    return;

                LoadedTableFileNamePair.FileName = UnsavedItem;
                Print($"You are using a new unsaved {nameof(Table)} for this game.", false,
                      ConsoleColor.DarkYellow);
                return;
            }
            saveSuccessful = true;
            Print($"You are using the newly saved '{fileName}' with '{LoadedCardCollectionFileName}' as the deck of cards for this game.", false, ConsoleColor.DarkGreen);
        }

        private void StartNewGame()
        {
            Print("Start new game\n", false);

            if (LoadedTable == null)
            {
                LoadedTableFileNamePair.Table = LoadedCardCollection != null
                    ? new Table(LoadedCardCollection.ToCardCollection())
                    : new Table(new CardCollection());
                bool saveSuccessful;
                string fileName;
                SaveToXml(out saveSuccessful, out fileName);
                LoadedTableFileNamePair.FileName = fileName;
                if (!saveSuccessful)
                    return;
            }
            else
            {
                Print($"You are using the loaded '{LoadedTableFileName}' for this game.", false, ConsoleColor.DarkGreen);
                PrintLaidCards();
            }

            while (!LoadedTable.EndOfGame)
            {
                if (LoadedTable.CanTakeNewCard)
                {
                    LoadedTable.TakeNewCard();
                    Print($"Took new card: {LoadedTable.LaidCards[LoadedTable.LaidCards.Count - 1]}", false);
                    PrintLaidCards();
                    if (QuitGame())
                        return;
                    if (LoadedTable.EndOfGame)
                        break;
                }
                if (LoadedTable.CanTakeNewCard)
                    continue;

                Card movableCard;
                int indexOfmovableCard = 0;
                CardMove cardMove;
                if (LoadedTable.MovableCards.Count == 1)
                    movableCard = LoadedTable.MovableCards[indexOfmovableCard].Card;
                else
                {
                    if (!TryGetMovableCard(out movableCard, out indexOfmovableCard))
                    {
                        if (QuitGame("If not, the first movable card will be selected."))
                            return;
                        indexOfmovableCard = 0;
                        movableCard = LoadedTable.MovableCards[indexOfmovableCard].Card;
                    }
                }
                if (LoadedTable.MovableCards[indexOfmovableCard].CardPositions.OneStepBack != null &&
                    LoadedTable.MovableCards[indexOfmovableCard].CardPositions.ThreeStepsBack != null)
                {
                    if (!TryGetCardMove(out cardMove, movableCard))
                    {
                        if (QuitGame("If not, the movable card will be moved three steps back."))
                            return;
                        cardMove = CardMove.ThreeStepsBack;
                    }
                }
                else
                    cardMove = LoadedTable.MovableCards.GetCardPositionsBy(movableCard).OneStepBack != null
                        ? CardMove.OneStepBack
                        : CardMove.ThreeStepsBack;
                switch (cardMove)
                {
                    case CardMove.OneStepBack:
                        Print(
                            $"Moved '{movableCard}' one step back to '{GetMatchingCardToMovableCard(movableCard, cardMove)}'",
                            false);
                        LoadedTable.MoveOneStepBack(movableCard);
                        PrintLaidCards();
                        if (QuitGame())
                            return;
                        break;
                    case CardMove.ThreeStepsBack:
                        Print(
                            $"Moved '{movableCard}' three steps back to '{GetMatchingCardToMovableCard(movableCard, cardMove)}'",
                            false);
                        LoadedTable.MoveThreeStepsBack(movableCard);
                        PrintLaidCards();
                        if (QuitGame())
                            return;
                        break;
                }
            }
            if (LoadedTable.LaidCards.Count == 1)
                Print("You won!", false, ConsoleColor.DarkGreen);
            else
                Print("You lost.", false, ConsoleColor.DarkRed);
            PrintReturningToMainMenu();
        }

        private bool QuitGame(string addToPromt = null)
        {
            if (!string.IsNullOrWhiteSpace(addToPromt) && !addToPromt.StartsWith(" "))
                addToPromt = addToPromt.Insert(0, " ");
            if (Console.ReadKey(true).Key != ConsoleKey.Escape || !Confirm($"Do you want to quit the game?{addToPromt ?? ""}", true))
                return false;

            string fileName,
                   message;
            Exception exception;
            if (!TrySaveLoadedTableToXml(out fileName, out message, out exception))
            {
                PrintError(message, exception, false);
                bool quitGame = Confirm("Are you sure that you want to quit the game?", true);
                PrintReturningToMainMenu();
                return quitGame;
            }
            PrintReturningToMainMenu(message, null, false);
            return true;
        }

        private bool TryGetMovableCard(out Card movableCard, out int indexOfMovableCard)
        {
            ConsoleKey answer;
            bool startAtZero = false;
            Dictionary<ConsoleKey, string> optionsDictionary =
                CreateOptionsDictionary(GetDigitKeys(LoadedTable.MovableCards.Count, startAtZero),
                    LoadedTable.MovableCards.ToStringArray());
            AskFor(out answer, "Which card do you want to move?", new OptionsDictionary(optionsDictionary));
            if (answer == ConsoleKey.Escape)
            {
                Print("Selection of card was aborted.", false);
                movableCard = null;
                indexOfMovableCard = -1;
                return false;
            }
            movableCard = LoadedTable.MovableCards[NumberKeyToInt(answer) - (startAtZero ? 0 : 1)].Card;
            indexOfMovableCard = LoadedTable.MovableCards.IndexOf(movableCard);
            return true;
        }

        private bool TryGetCardMove(out CardMove cardMove, Card cardToMove)
        {
            ConsoleKey answer;
            Dictionary<ConsoleKey, string> optionsDictionary = new Dictionary<ConsoleKey, string>()
            {
                {ConsoleKey.D1, $"One step back to '{GetMatchingCardToMovableCard(cardToMove, CardMove.OneStepBack)}'"},
                {ConsoleKey.D3, $"Three steps back to '{GetMatchingCardToMovableCard(cardToMove, CardMove.ThreeStepsBack)}'"}
            };
            AskFor(out answer, $"Where do you want to move the card '{cardToMove}'?", new OptionsDictionary(optionsDictionary));
            if (answer == ConsoleKey.D1)
            {
                cardMove = CardMove.OneStepBack;
                return true;
            }
            if (answer == ConsoleKey.D3)
            {
                cardMove = CardMove.ThreeStepsBack;
                return true;
            }

            Print("Selection of card was aborted.", false);
            cardMove = CardMove.ThreeStepsBack;
            return false;
        }

        private Card GetMatchingCardToMovableCard(Card movableCard, CardMove atPosition)
        {
            int indexOfMatchingCard = LoadedTable.LaidCards.IndexOf(movableCard);
            switch (atPosition)
            {
                case CardMove.OneStepBack:
                    indexOfMatchingCard -= 1;
                    break;
                case CardMove.ThreeStepsBack:
                    indexOfMatchingCard -= 3;
                    break;
            }
            return LoadedTable.LaidCards[indexOfMatchingCard];
        }

        private void PrintLaidCards()
        {
            if (LoadedTable.LaidCards == null)
                throw new Exception("Cannot print an collection of cards that is null.");
            if (LoadedTable.LaidCards.Count == 0)
                throw new Exception("Cannot print an empty collection of cards.");

            const int columns = 8;
            int rows = SetRows();

            List<Tuple<Card, ConsoleColor>> laidCards = SetCardPrintColor(LoadedTable.LaidCards.ToList());

            for (int row = 1; row <= rows; row++)
            {
                List<Tuple<Card, ConsoleColor>> cards = laidCards.Take(laidCards.Count >= columns ? columns : laidCards.Count).ToList();
                if (row % 2 != 0)
                    PrintOddRow(cards);
                else
                    PrintEvenRow(cards);

                Console.WriteLine();
                laidCards.RemoveRange(0, row == rows ? laidCards.Count : columns);
            }
        }

        private int SetRows()
        {
            int rows;
            if (LoadedTable.LaidCards.Count <= 8)
                rows = 1;
            else if (LoadedTable.LaidCards.Count <= 16)
                rows = 2;
            else if (LoadedTable.LaidCards.Count <= 24)
                rows = 3;
            else if (LoadedTable.LaidCards.Count <= 32)
                rows = 4;
            else if (LoadedTable.LaidCards.Count <= 40)
                rows = 5;
            else if (LoadedTable.LaidCards.Count <= 48)
                rows = 6;
            else
                rows = 7;

            return rows;
        }

        private void PrintOddRow(List<Tuple<Card, ConsoleColor>> cards)
        {
            PrintCardsWithColors(cards);
        }

        private void PrintEvenRow(List<Tuple<Card, ConsoleColor>> cards)
        {
            cards.Reverse();
            if (cards.Count() < 8)
            {
                int missingCards = 8 - cards.Count;
                for (int i = 0; i < missingCards; i++)
                    cards.Insert(0, null);
            }
            PrintCardsWithColors(cards);
        }

        private List<Tuple<Card, ConsoleColor>> SetCardPrintColor(List<Card> cards)
        {
            List<Tuple<Card, ConsoleColor>> cardsWithColors = new List<Tuple<Card, ConsoleColor>>();
            foreach (Card card in cards)
            {
                cardsWithColors.Add(new Tuple<Card, ConsoleColor>(card, ConsoleColor.Black));
            }
            if (LoadedTable.MovableCards.IsEmpty)
                return cardsWithColors;

            int movableCardsCounter = 0;
            foreach (CardCardPositionsPair ccpp in LoadedTable.MovableCards)
            {
                int index = cards.IndexOf(ccpp.Card);
                if (index < 0)
                    continue;

                cardsWithColors[index] = new Tuple<Card, ConsoleColor>(ccpp.Card, _movableCardColors[movableCardsCounter]);
                if (ccpp.CardPositions.OneStepBack != null)
                    cardsWithColors[(int)ccpp.CardPositions.OneStepBack] = new Tuple<Card, ConsoleColor>(cards[(int)ccpp.CardPositions.OneStepBack], _matchingCardsColors[movableCardsCounter]);
                if (ccpp.CardPositions.ThreeStepsBack != null)
                    cardsWithColors[(int)ccpp.CardPositions.ThreeStepsBack] = new Tuple<Card, ConsoleColor>(cards[(int)ccpp.CardPositions.ThreeStepsBack], _matchingCardsColors[movableCardsCounter]);
                movableCardsCounter++;
            }
            return cardsWithColors;
        }

        private void PrintCardsWithColors(List<Tuple<Card, ConsoleColor>> cardsWithColors)
        {
            if (cardsWithColors == null)
                throw new Exception("Cannot print a list that is null.");
            if (cardsWithColors.Count == 0)
                throw new Exception("Cannot print a empty list.");

            foreach (Tuple<Card, ConsoleColor> tuple in cardsWithColors)
            {
                if (tuple == null)
                {
                    Console.ResetColor();
                    Console.Write("     ");
                }
                else
                {
                    Console.BackgroundColor = tuple.Item2;
                    Console.ForegroundColor = tuple.Item2 == ConsoleColor.Black ? ConsoleColor.White : ConsoleColor.Black;
                    Console.Write("{0,2}:{1} ", GetFaceCardDenomination(tuple.Item1.Denomination), tuple.Item1.Suit.ToString().First());
                }
            }

            Console.ResetColor();
        }

        private string GetFaceCardDenomination(CardDenomination cardDenomination)
        {
            switch (cardDenomination)
            {
                case CardDenomination.Jack:
                    return "J";
                case CardDenomination.Queen:
                    return "Q";
                case CardDenomination.King:
                    return "K";
                case CardDenomination.Ace:
                    return "A";
                default:
                    return ((int)cardDenomination).ToString();
            }
        }

        private void LoadFile()
        {
            Print("Load existing file:\n", false, ConsoleColor.DarkGreen);

            string itemDirectory = GetItemDirectory();
            if (itemDirectory == null)
            {
                PrintReturningToMainMenu("Load selection aborted.");
                return;
            }
            Item item = (Item)Enum.Parse(typeof(Item), itemDirectory);
            Console.Clear();
            string itemTypeDirectory = GetItemTypeDirectory(item.ToString());
            if (itemTypeDirectory == null)
            {
                PrintReturningToMainMenu("Load selection aborted.");
                return;
            }
            Console.Clear();

            string path;
            Exception exception;
            if (!TryGetPath.ToDirectory(new[] { MainDirectory, itemDirectory, itemTypeDirectory }, false, out path, out exception))
            {
                PrintReturningToMainMenu("Could not get directory path.", exception);
                return;
            }

            try
            {
                bool loadAnOtherFile = true;
                while (loadAnOtherFile)
                {
                    string[] filePaths = Directory.GetFiles(path, "*.xml");
                    if (filePaths.Length == 0)
                    {
                        PrintReturningToMainMenu($"Could not find any {item}'s in:\n{path}");
                        return;
                    }
                    Print($"Found {item}'s in:\n{path + "\\"}", false, ConsoleColor.DarkGreen);
                    int answer;
                    const bool startOptionAtZero = false;
                    AskFor(out answer, "Enter the number of the file that you want to load",
                           new OptionsStringArray(filePaths.ReplaceInAll(path + "\\", ""), startOptionAtZero),
                           new IntInputRange(1, filePaths.Length, -1));
                    if (answer == -1)
                    {
                        PrintReturningToMainMenu("No file selected.");
                        return;
                    }
                    answer = startOptionAtZero ? answer : answer - 1;
                    string fileName = Path.GetFileName(filePaths[answer]);
                    CardCollection cardCollection = null;
                    Table table = null;
                    switch (item)
                    {
                        case Item.CardCollection:
                            if (!XmlHandler.TryLoadFrom(filePaths[answer], out cardCollection, out exception))
                            {
                                PrintReturningToMainMenu($"Could not load: {fileName}", exception);
                                return;
                            }
                            LoadedCardCollectionFileNamePair.SetValues(cardCollection, fileName);
                            LoadedTableFileNamePair.SetValues(null, null);
                            break;
                        case Item.Table:
                            if (!XmlHandler.TryLoadFrom(filePaths[answer], out table, out exception))
                            {
                                PrintReturningToMainMenu($"Could not load: {fileName}", exception);
                                return;
                            }
                            LoadedTableFileNamePair.SetValues(table, fileName);
                            LoadedCardCollectionFileNamePair.SetValues(null, null);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    Print($"Successfully loaded:{fileName}", false, ConsoleColor.DarkGreen);

                    if (Confirm("Do you want to view the content?"))
                    {
                        Console.WriteLine("\n=====================");
                        switch (item)
                        {
                            case Item.CardCollection:
                                Console.WriteLine(cardCollection.ToString(true));
                                break;
                            case Item.Table:
                                Console.WriteLine(table.ToString());
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        Console.WriteLine("=====================\n");
                    }

                    loadAnOtherFile = Confirm("Do you want to load an other file?");
                    if (loadAnOtherFile)
                        Console.Clear();
                }
                PrintReturningToMainMenu();
            }
            catch (Exception e)
            {
                PrintReturningToMainMenu($"Could not get files in:\n{path}", e);
            }
        }

        private string GetItemDirectory()
        {
            Item? item = GetItemFromInput("What kind of item do you want to load?");
            switch (item)
            {
                case Item.CardCollection:
                    return Item.CardCollection.ToString();
                case Item.Table:
                    return Item.Table.ToString();
                case null:
                    return null;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Item? GetItemFromInput(string prompt)
        {
            ConsoleKey answer;
            AskFor(out answer, prompt,
                   new OptionsDictionary(CreateOptionsDictionary(
                                                                 new[] { ConsoleKey.C, ConsoleKey.T },
                                                                 new[] { Item.CardCollection.ToString(), Item.Table.ToString() })));

            switch (answer)
            {
                case ConsoleKey.C:
                    return Item.CardCollection;
                case ConsoleKey.T:
                    return Item.Table;
                default:
                    return null;
            }
        }

        private string GetItemTypeDirectory(string item)
        {
            ConsoleKey answer;
            AskFor(out answer, $"What kind of {item} type do you want to load?",
                   new OptionsDictionary(CreateOptionsDictionary(
                                                                 new[] { ConsoleKey.D, ConsoleKey.P, ConsoleKey.C },
                                                                 new[] { ItemType.Debug.ToString(), ItemType.Played.ToString(), ItemType.ContainingBugs.ToString() })));
            if (answer == ConsoleKey.Escape)
                return null;
            switch (answer)
            {
                case ConsoleKey.D:
                    return ItemType.Debug.ToString();
                case ConsoleKey.P:
                    return ItemType.Played.ToString();
                case ConsoleKey.C:
                    return ItemType.ContainingBugs.ToString();
                default:
                    return null;
            }
        }

        private void ViewLoadedItem()
        {
            Print("View loaded item:\n", false, ConsoleColor.DarkGreen);

            bool viewAnOtherItem = true;
            while (viewAnOtherItem)
            {
                Item? item = GetItemFromInput("What kind of item do you want to view?");
                string itemFileName = "",
                    itemString = "";
                switch (item)
                {
                    case null:
                        PrintReturningToMainMenu("No item selected.");
                        return;
                    case Item.CardCollection:
                        if (LoadedCardCollection == null)
                        {
                            PrintError($"Loaded {nameof(CardCollection)} is null.", null, false);
                            if (Confirm("Do you want to view an other item?"))
                                continue;
                            PrintReturningToMainMenu();
                            return;
                        }
                        itemFileName = LoadedCardCollectionFileName;
                        itemString = LoadedCardCollection.ToString(true);
                        break;
                    case Item.Table:
                        if (LoadedTable == null)
                        {
                            PrintError($"Loaded {nameof(Table)} is null.", null, false);
                            if (Confirm("Do you want to view an other item?"))
                                continue;
                            PrintReturningToMainMenu();
                            return;
                        }
                        itemFileName = LoadedTableFileName;
                        itemString = LoadedTable.ToString();
                        break;
                }
                Print(itemFileName, false, ConsoleColor.DarkGreen);
                Console.WriteLine("\n=====================");
                Console.WriteLine(itemString);
                Console.WriteLine("=====================\n");
                viewAnOtherItem = Confirm("Do you want to view an other item?");
            }
            PrintReturningToMainMenu();
        }

        private bool TrySaveLoadedTableToXml(out string fileName, out string message, out Exception exception)
        {
            if (LoadedTable == null)
            {
                fileName = null;
                message = "Could not save xml file.";
                exception = new ArgumentNullException($"{nameof(LoadedTable)} cannot be null.");
                return false;
            }

            string name = LoadedTableFileName == null
                          ? $"{nameof(Table)}_{DateTime.Now:yyyy-MM-dd_HH.mm.ss}"
                          : Path.GetFileNameWithoutExtension(LoadedTableFileName);

            string filePath;
            if (TryGetPath.ToFile(name, new[] { MainDirectory, Item.Table.ToString(), ItemType.Played.ToString() },
                false, out filePath, out exception))
            {
                if (!File.Exists(filePath + GetFileEnding(0)))
                    filePath += GetFileEnding(0);
                else
                {
                    if (Confirm($"Do you want to overwrite the existing file: {Path.GetFileName(filePath + GetFileEnding(0))}"))
                    {
                        uint fileCounter = 1;
                        while (File.Exists(filePath + GetFileEnding(fileCounter)) || fileCounter < 10)
                        {
                            fileCounter++;
                        }
                        filePath += GetFileEnding(fileCounter);
                    }
                }
                if (XmlHandler.TrySaveTo(filePath, LoadedTable, out exception))
                {
                    fileName = Path.GetFileName(filePath);
                    message = $"Saved xml file at:\n{filePath}";
                    exception = null;
                    return true;
                }

                message = $"Could not save xml file at:\n{filePath}";
            }
            else
                message = "Could not get file path.";

            fileName = null;
            return false;
        }

        private string GetFileEnding(uint fileNumber)
        {
            StringBuilder outBuilder = new StringBuilder();
            if (fileNumber != 0)
            {
                outBuilder.Append("_");
                outBuilder.Append(fileNumber);
            }
            outBuilder.Append(".xml");

            return outBuilder.ToString();
        }

        private void UnloadItem()
        {
            Print("Unloaded item:\n", false, ConsoleColor.DarkGreen);

            Item? item = GetItemFromInput("What kind of item do you want to unload?");

            switch (item)
            {
                case null:
                    PrintReturningToMainMenu("No item selected.");
                    return;
                case Item.CardCollection:
                    if (LoadedCardCollection == null)
                    {
                        PrintReturningToMainMenu($"Loaded {nameof(CardCollection)} is already null.");
                        return;
                    }
                    LoadedCardCollectionFileNamePair.CardCollection = null;
                    PrintReturningToMainMenu($"Unloaded: {LoadedCardCollectionFileName}", null, false);
                    LoadedCardCollectionFileNamePair.FileName = null;
                    return;
                case Item.Table:
                    if (LoadedTable == null)
                    {
                        PrintReturningToMainMenu($"Loaded {nameof(Table)} is already null.");
                        return;
                    }
                    LoadedTableFileNamePair.Table = null;
                    PrintReturningToMainMenu($"Unloaded: {LoadedTableFileName}", null, false);
                    LoadedTableFileNamePair.FileName = null;
                    return;
            }
        }

        private void Quit()
        {
            bool quit;
            AskFor(out quit, "Do you really want to quit?");
            if (quit)
                EndLoop();
        }
    }
}
