﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using LangLojan.Utilities;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojan.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private readonly Table _table = new Table(new CardCollection());

        private ObservableCollection<Tuple<Card, CardStatus>> _laidCards;
        public ObservableCollection<Tuple<Card, CardStatus>> LaidCards
        {
            get { return _laidCards; }
            set { SetField(ref _laidCards, value); }
        }

        private Tuple<Card, CardStatus> _selectedCard;
        public Tuple<Card, CardStatus> SelectedCard
        {
            get { return _selectedCard; }
            set
            {
                SetField(ref _selectedCard, value);
                MoveActiveCard();
            }
        }

        private bool[] _activeCardHasMatch = new bool[2];
        public bool[] ActiveCardHasMatch
        {
            get { return _activeCardHasMatch; }
            set { SetField(ref _activeCardHasMatch, value); }
        }

        private CardStatus _statusOfCard;

        public CardStatus StatusOfCard
        {
            get { return _statusOfCard; }
            set { SetField(ref _statusOfCard, value); }
        }

        public ICommand TakeNewCardCommand { get; set; }
        //public ICommand MoveActiveCardCommand { get; set; }

        public MainViewModel()
        {
            LoadCommands();
        }

        private void LoadCommands()
        {
            TakeNewCardCommand = new CustomCommand(TakeNewCard, CanTakeNewCard);
            //MoveActiveCardCommand = new CustomCommand(MoveActiveCard, CanMoveActiveCard);
        }

        private void TakeNewCard(object obj)
        {
            _table.TakeNewCard();
            SetLaidCards();
        }

        private bool CanTakeNewCard(object obj)
        {
            return _table.CanTakeNewCard;
        }

        private void SetLaidCards()
        {
            ObservableCollection<Tuple<Card, CardStatus>> listWithTuples = new ObservableCollection<Tuple<Card, CardStatus>>();
            foreach (Card card in _table.LaidCards)
            {
                //if (card.Equals(_table.ActiveCard))
                //    listWithTuples.Add(new Tuple<Card, CardStatus>(card, CardStatus.Movable));
                //else if (card.Equals(_table.CardOneStepBack) && _table.ActiveCardHasMatchOneStepBack)
                //    listWithTuples.Add(new Tuple<Card, CardStatus>(card, CardStatus.MatchingOneStepBack));
                //else if (card.Equals(_table.CardThreeStepsBack) && _table.ActiveCardHasMatchThreeStepsBack)
                //    listWithTuples.Add(new Tuple<Card, CardStatus>(card, CardStatus.MatchingThreeStepsBack));
                //else
                //    listWithTuples.Add(new Tuple<Card, CardStatus>(card, CardStatus.Other));
            }

            SetActiveCardHasMatch();
            LaidCards = listWithTuples;
        }

        private void SetActiveCardHasMatch()
        {
            //ActiveCardHasMatch[0] = _table.ActiveCardHasMatchOneStepBack;
            //ActiveCardHasMatch[1] = _table.ActiveCardHasMatchThreeStepsBack;
        }

        private void MoveActiveCard()
        {
            if (SelectedCard == null)
                return;
            if (!CanMoveActiveCard())
                return;

            //if (SelectedCard.Item1.Equals(_table.CardOneStepBack) &&
            //    SelectedCard.Item2 == CardStatus.MatchingOneStepBack)
            //    _table.MoveActiveCardOneStepBack();
            //else if (SelectedCard.Item1.Equals(_table.CardThreeStepsBack) &&
            //         SelectedCard.Item2 == CardStatus.MatchingThreeStepsBack)
            //    _table.MoveActiveCardThreeStepsBack();

            //while (_table.CanUpdateActiveCard)
            //    _table.UpdateActiveCard();

            SetLaidCards();
        }

        private bool CanMoveActiveCard()
        {
            //if (_table.CardOneStepBack == null)
            //    return false;
            //if (SelectedCard.Item1.Equals(_table.CardOneStepBack))
            //    return true;
            //if (_table.CardThreeStepsBack == null)
            //    return false;
            //if (SelectedCard.Item1.Equals(_table.CardThreeStepsBack))
            //    return true;

            return false;
        }
    }
}
