﻿using System;
using System.Globalization;
using System.Windows.Data;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojan.Converter
{
    public class CardStatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "Black";

            CardStatus cardStatus = ((Tuple<Card, CardStatus>) value).Item2;
            switch (cardStatus)
            {
                case CardStatus.Movable:
                    return "Blue";
                case CardStatus.MatchingOneStepBack:
                    return "Yellow";
                case CardStatus.MatchingThreeStepsBack:
                    return "Yellow";
            }

            return "Transparent";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}