﻿using System;
using System.Windows;
using System.Windows.Data;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojan.Converter
{
    public class CardToResourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Application.Current.FindResource("JokerColor");

            Card card = ((Tuple<Card, CardStatus>)value).Item1;
            string cardResource = card.Denomination.ToString() + "Of" + card.Suit.ToString();
            return Application.Current.FindResource(cardResource);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}