﻿using LangLojan.ViewModels;

namespace LangLojan.Utilities
{
    public class ViewModelLocator
    {
        public static MainViewModel MainViewModel { get; } = new MainViewModel();
        public static TableViewModel TableViewModel { get; } = new TableViewModel();
    }
}
