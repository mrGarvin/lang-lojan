﻿using System.Collections.Generic;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface ICardCollection : IObservableCardCollection, IXmlItem
    {
        Card TakeFirst();
        void AddToTop(Card card);
        void AddToTop(IEnumerable<Card> cards);
    }
}