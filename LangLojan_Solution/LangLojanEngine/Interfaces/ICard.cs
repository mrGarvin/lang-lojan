﻿using LangLojanEngine.Domain.Enums;

namespace LangLojanEngine.Interfaces
{
    public interface ICard : IXmlItem
    {
        CardDenomination Denomination { get; }
        CardSuit Suit { get; }
    }
}