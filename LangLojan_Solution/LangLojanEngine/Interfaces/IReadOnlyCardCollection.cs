﻿using LangLojanEngine.Collections;

namespace LangLojanEngine.Interfaces
{
    public interface IReadOnlyCardCollection : IObservableCardCollection
    {
        int MaxCapacity { get; }
        int NumberOfSuits { get; }
        int MinNumberOfDenominations { get; }
        int MaxNumberOfDenominations { get; }
        CardCollection ToCardCollection();
    }
}