﻿using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace LangLojanEngine.Interfaces
{
    public interface IXElementContainer
    {
        XElement XElement { get; }
    }
}