﻿using System.Collections.Generic;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface IObservableCardCollection : IEnumerable<Card>
    {
        Card this[int index] { get; }
        int Count { get; }
        bool IsShuffled { get; }
        bool IsEmpty { get; }
        bool IsFull { get; }
        string ToString(bool newLineAsCardSeparator);
    }
}