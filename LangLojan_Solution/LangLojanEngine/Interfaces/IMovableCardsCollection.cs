﻿using System.Collections.Generic;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface IMovableCardsCollection : IObservableMovableCardsCollection, IXmlItem
    {
        void Add(Card card, CardPositions cardPositions);
        void Remove(Card card);
        void RemoveCardAt(int index);
        void Clear();
    }
}