﻿using System.Xml.Linq;

namespace LangLojanEngine.Interfaces
{
    public interface IXmlItem
    {
        XElement ToXml(string name = null);
    }
}