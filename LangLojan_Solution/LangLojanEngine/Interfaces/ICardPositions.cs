﻿using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface ICardPositions : IXmlItem
    {
        uint? OneStepBack { get; }
        uint? ThreeStepsBack { get; }
    }
}