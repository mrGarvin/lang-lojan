﻿using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface ICardCardPositionsPair : IXmlItem
    {
        Card Card { get; }
        CardPositions CardPositions { get; }
    }
}