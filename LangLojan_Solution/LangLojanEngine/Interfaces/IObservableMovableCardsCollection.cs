﻿using System.Collections.Generic;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Interfaces
{
    public interface IObservableMovableCardsCollection : IEnumerable<CardCardPositionsPair>
    {
        CardCardPositionsPair this[int index] { get; }
        int Count { get; }
        bool IsEmpty { get; }
        bool IsFull { get; }

        bool Contains(Card card);
        bool Contains(CardPositions cardPositions);
        bool Contains(uint position);
        Card[] GetAllCards();
        CardPositions[] GetAllCardPositions();
        CardPositions GetCardPositionsBy(Card card);
        int IndexOf(Card card);
        string[] ToStringArray();
        string ToString(bool newLineAsCardSeparator);
    }
}