﻿using LangLojanEngine.Collections;

namespace LangLojanEngine.Interfaces
{
    public interface IReadOnlyMovableCardsCollection : IObservableMovableCardsCollection
    {
        int MaxCapacity { get; }
        MovableCardsCollection ToMovableCardsCollection();
    }
}