﻿namespace LangLojanEngine.Domain.Enums
{
    public enum CardMove
    {
        OneStepBack = 1,
        ThreeStepsBack = 3
    }
}