﻿namespace LangLojanEngine.Domain.Enums
{
    public enum CardSuit
    {
        Hearts,
        Spades,
        Diamonds,
        Clubs
    }
}