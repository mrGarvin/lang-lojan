﻿namespace LangLojanEngine.Domain.Enums
{
    public enum CardStatus
    {
        Movable,
        MatchingOneStepBack,
        MatchingThreeStepsBack,
        Other
    }
}