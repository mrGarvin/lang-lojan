﻿using System;
using System.Linq;
using System.Xml.Linq;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Domain.ValueObjects
{
    public struct CardPositions : ICardPositions
    {
        public uint? OneStepBack { get; }
        public uint? ThreeStepsBack { get; }

        public CardPositions(uint? oneStepBack, uint? threeStepsBack)
        {
            if (oneStepBack == null && threeStepsBack == null)
                throw new ArgumentException($"Both {nameof(oneStepBack)} and {nameof(threeStepsBack)} cannot be null.");
            if (oneStepBack == threeStepsBack)
                throw new ArgumentNullException($"{nameof(oneStepBack)} cannot be equal to {nameof(threeStepsBack)}.");
            if (oneStepBack < threeStepsBack)
                throw new ArgumentNullException($"{nameof(oneStepBack)} cannot be less than {nameof(threeStepsBack)}.");

            OneStepBack = oneStepBack;
            ThreeStepsBack = threeStepsBack;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            try
            {
                CardPositions other = (CardPositions)obj;
                return OneStepBack == other.OneStepBack && ThreeStepsBack == other.ThreeStepsBack;
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (OneStepBack.GetHashCode()*397) ^ ThreeStepsBack.GetHashCode();
            }
        }

        public override string ToString()
        {
            return $"{(OneStepBack == null ? "null" : OneStepBack.ToString())}, {(ThreeStepsBack == null ? "null" : ThreeStepsBack.ToString())}";
        }

        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(CardPositions);

            XElement xElement = new XElement(name);
            xElement.Add(new XAttribute(nameof(OneStepBack), OneStepBack == null ? "null" : OneStepBack.ToString()));
            xElement.Add(new XAttribute(nameof(ThreeStepsBack), ThreeStepsBack == null ? "null" : ThreeStepsBack.ToString()));
            return xElement;
        }

        public CardPositions(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidAttributes(xElementContainer.XElement,
                                               new[] {nameof(OneStepBack), nameof(ThreeStepsBack)}, 
                                               out exception))
                throw exception;

            string oneStepBackString = xElementContainer.XElement.Attribute(nameof(OneStepBack)).Value,
                   threeStepsBackString = xElementContainer.XElement.Attribute(nameof(ThreeStepsBack)).Value;

            uint? oneStepBack,
                  threeStepsBack;

            if (!TryParseToNullableUint(oneStepBackString, out oneStepBack))
                throw new ArgumentException($"{nameof(oneStepBackString)} is not a valid string.");
            if (!TryParseToNullableUint(threeStepsBackString, out threeStepsBack))
                throw new ArgumentException($"{nameof(threeStepsBackString)} is not a valid string.");
            try
            {
                CardPositions cardPositions = new CardPositions(oneStepBack, threeStepsBack);
                OneStepBack = cardPositions.OneStepBack;
                ThreeStepsBack = cardPositions.ThreeStepsBack;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        private static bool TryParseToNullableUint(string s, out uint? result)
        {
            if (s == "null")
            {
                result = null;
                return true;
            }
            uint r;
            if (uint.TryParse(s, out r))
            {
                result = r;
                return true;
            }
            result = 0;
            return false;
        }
    }
}