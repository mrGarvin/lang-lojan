﻿using System;
using System.Linq;
using System.Xml.Linq;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Domain.ValueObjects
{
    public struct CardCardPositionsPair : ICardCardPositionsPair
    {
        public Card Card { get; }
        public CardPositions CardPositions { get; }

        public CardCardPositionsPair(Card card, CardPositions cardPositions)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            Card = card;
            CardPositions = cardPositions;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            try
            {
                CardCardPositionsPair other = (CardCardPositionsPair) obj;
                return Equals(Card, other.Card) && CardPositions.Equals(other.CardPositions);
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Card?.GetHashCode() ?? 0)*397) ^ CardPositions.GetHashCode();
            }
        }

        public override string ToString()
        {
            return $"{Card}: {CardPositions}";
        }

        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(CardCardPositionsPair);

            XElement xElement = new XElement(name);
            xElement.Add(Card.ToXml());
            xElement.Add(CardPositions.ToXml());
            return xElement;
        }

        public CardCardPositionsPair(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidElements(xElementContainer.XElement, 
                                             new[] {nameof(Card), nameof(CardPositions)},
                                             out exception))
                throw exception;
            try
            {
                Card = new Card(new XElementContainer(xElementContainer.XElement.Element(nameof(Card))));
                CardPositions = new CardPositions(new XElementContainer(xElementContainer.XElement.Element(nameof(CardPositions))));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}