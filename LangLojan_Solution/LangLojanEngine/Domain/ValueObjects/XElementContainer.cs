﻿using System;
using System.Xml.Linq;
using LangLojanEngine.Interfaces;

namespace LangLojanEngine.Domain.ValueObjects
{
    public struct XElementContainer : IXElementContainer
    {
        public XElement XElement { get; }

        public XElementContainer(XElement xElement)
        {
            if (xElement == null)
                throw new ArgumentNullException($"{nameof(xElement)} cannot be null.");
            XElement = xElement;
        }
    }
}