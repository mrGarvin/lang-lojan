﻿using System;
using System.Linq;
using System.Xml.Linq;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Domain.ValueObjects
{
    public class Card : ICard
    {
        public CardDenomination Denomination { get; }
        public CardSuit Suit { get; }

        public Card(CardDenomination denomination, CardSuit suit)
        {
            Denomination = denomination;
            Suit = suit;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            try
            {
                Card other = (Card)obj;
                return Denomination == other.Denomination && Suit == other.Suit;
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)Denomination * 397) ^ (int)Suit;
            }
        }

        public override string ToString()
        {
            return $"{Denomination} of {Suit}";
        }
        
        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(Card);

            XElement rcElement = new XElement(name);
            rcElement.Add(new XAttribute(nameof(Denomination), Denomination.ToString()));
            rcElement.Add(new XAttribute(nameof(Suit), Suit.ToString()));
            return rcElement;
        }

        public Card(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidAttributes(xElementContainer.XElement, 
                                               new[] {nameof(Denomination), nameof(Suit)},
                                               out exception))
                throw exception;

            string demoninationString = xElementContainer.XElement.Attribute(nameof(Denomination)).Value,
                   suitString = xElementContainer.XElement.Attribute(nameof(Suit)).Value;
            CardDenomination cDenomination;
            CardSuit cSuit;

            if (!Enum.TryParse(demoninationString, out cDenomination))
                throw new ArgumentException($"{nameof(demoninationString)} is not a valid string.");
            if (!Enum.TryParse(suitString, out cSuit))
                throw new ArgumentException($"{nameof(suitString)} is not a valid string.");

            Denomination = cDenomination;
            Suit = cSuit;
        }
    }
}