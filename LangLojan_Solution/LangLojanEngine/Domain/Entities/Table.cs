﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Domain.Entities
{
    public class Table : IXmlItem
    {
        public Guid Id { get; }
        public DateTime Created { get; }
        public DateTime LastPlayed { get; }
        public ReadOnlyCardCollection DeckOfCards => new ReadOnlyCardCollection(_deckOfCards);
        private readonly CardCollection _deckOfCards;
        public ReadOnlyCollection<Card> LaidCards => new ReadOnlyCollection<Card>(_laidCards);
        private readonly List<Card> _laidCards;
        public ReadOnlyMovableCardsCollection MovableCards => new ReadOnlyMovableCardsCollection(_movableCards);
        private readonly MovableCardsCollection _movableCards;
        public bool CanTakeNewCard => _movableCards.IsEmpty && !_deckOfCards.IsEmpty;
        public bool EndOfGame => _movableCards.IsEmpty && _deckOfCards.IsEmpty;

        public Table(CardCollection deckOfCards)
        {
            if (deckOfCards == null)
                throw new ArgumentNullException(
                    $"Cannot create a new {nameof(Table)} because {nameof(deckOfCards)} is null.");
            if (deckOfCards.IsEmpty)
                throw new ArgumentException(
                    $"Cannot create a new {nameof(Table)} because {nameof(deckOfCards)} is empty.");

            Id = Guid.NewGuid();
            Created = DateTime.Now;
            LastPlayed = Created;
            _deckOfCards = deckOfCards;
            _laidCards = new List<Card>(_deckOfCards.Count);
            _movableCards = new MovableCardsCollection();
        }

        public void TakeNewCard()
        {
            if (_deckOfCards.IsEmpty)
                throw new Exception($"Cannot take a new card because {nameof(_deckOfCards)} is empty.");
            UpdateMovableCards();
            if (!CanTakeNewCard)
                throw new Exception($"Cannot take a new card because {nameof(_movableCards)} is not empty.");

            _laidCards.Add(_deckOfCards.TakeFirst());
            UpdateMovableCards();
        }

        public bool IsMovableOneStepBack(Card card)
        {
            if (card == null)
                return false;
            if (!_movableCards.Contains(card))
                return false;
            CardPositions cardPositions = _movableCards.GetCardPositionsBy(card);
            if (cardPositions.OneStepBack == null)
                return false;
            if (cardPositions.OneStepBack.Value >= _laidCards.Count)
                return false;

            return card.Match(_laidCards[Convert.ToInt32(cardPositions.OneStepBack.Value)]);
        }

        public bool IsMovableThreeStepsBack(Card card)
        {
            if (card == null)
                return false;
            if (!_movableCards.Contains(card))
                return false;
            CardPositions cardPositions = _movableCards.GetCardPositionsBy(card);
            if (cardPositions.ThreeStepsBack == null)
                return false;
            if (cardPositions.ThreeStepsBack.Value >= _laidCards.Count)
                return false;

            return card.Match(_laidCards[Convert.ToInt32(cardPositions.ThreeStepsBack.Value)]);
        }

        public bool IsMovable(Card card)
        {
            return IsMovableOneStepBack(card) || IsMovableThreeStepsBack(card);
        }

        public void MoveOneStepBack(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be moved because it is null.");
            if (!IsMovableOneStepBack(card))
                throw new ArgumentException(
                    $"{nameof(card)} cannot be moved because it does not have a match one step back.");

            int positionOneStepBack = Convert.ToInt32(_movableCards.GetCardPositionsBy(card).OneStepBack),
                positionOfMovableCard = _laidCards.IndexOf(card);
            _laidCards[positionOneStepBack] = _laidCards[positionOfMovableCard];
            _laidCards.RemoveAt(positionOfMovableCard);
            UpdateMovableCards();
        }

        public void MoveThreeStepsBack(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be moved because it is null.");
            if (!IsMovableThreeStepsBack(card))
                throw new ArgumentException(
                    $"{nameof(card)} cannot be moved because it does not have a match three steps back.");

            int positionThreeStepsBack = Convert.ToInt32(_movableCards.GetCardPositionsBy(card).ThreeStepsBack),
                positionOfMovableCard = _laidCards.IndexOf(card);
            _laidCards[positionThreeStepsBack] = _laidCards[positionOfMovableCard];
            _laidCards.RemoveAt(positionOfMovableCard);
            UpdateMovableCards();
        }

        private void UpdateMovableCards()
        {
            _movableCards.Clear();
            if (_laidCards.Count <= 1)
                return;

            for (int position = 1; position < _laidCards.Count; position++)
            {
                uint? positionOneStepBack = (uint?) (position - 1),
                    positionThreeStepsBack = position - 3 < 0 ? null : (uint?) (position - 3);

                Card card = _laidCards[position];
                bool matchOneBack = card.Match(_laidCards[(int) positionOneStepBack]),
                    matchThreeBack = positionThreeStepsBack != null &&
                                     card.Match(_laidCards[(int) positionThreeStepsBack]);
                if (matchOneBack || matchThreeBack)
                    _movableCards.Add(card, new CardPositions(matchOneBack ? positionOneStepBack : null,
                        matchThreeBack ? positionThreeStepsBack : null));
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(IdAttributeName);
            stringBuilder.Append(NameValueSeperator);
            stringBuilder.AppendLine(Id.ToString());

            stringBuilder.Append(CreatedAttributeName);
            stringBuilder.Append(NameValueSeperator);
            stringBuilder.AppendLine(Created.ToString());

            stringBuilder.Append(LastPlayedAttributeName);
            stringBuilder.Append(NameValueSeperator);
            stringBuilder.AppendLine(LastPlayed.ToString());
            stringBuilder.AppendLine();

            stringBuilder.Append(DeckOfCardsElementName);
            stringBuilder.AppendLine(NameValueSeperator);
            stringBuilder.AppendLine(_deckOfCards.ToString(true));

            stringBuilder.Append(LaidCardsElementName);
            stringBuilder.AppendLine(NameValueSeperator);
            stringBuilder.AppendLine(LaidCardsToString());

            stringBuilder.Append(MovableCardsElementName);
            stringBuilder.AppendLine(NameValueSeperator);
            stringBuilder.AppendLine(_movableCards.ToString(true));

            return stringBuilder.ToString();
        }

        private string LaidCardsToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Card card in _laidCards)
            {
                stringBuilder.AppendLine(card.ToString());
            }
            return stringBuilder.ToString();
        }

        private const string NameValueSeperator = ": ";

        private const string IdAttributeName = nameof(Id);
        private const string CreatedAttributeName = nameof(Created);
        private const string LastPlayedAttributeName = nameof(LastPlayed);
        private const string DeckOfCardsElementName = nameof(DeckOfCards);
        private const string LaidCardsElementName = nameof(LaidCards);
        private const string MovableCardsElementName = nameof(MovableCards);

        private string DeckOfCardsFileName { get; }

        private XElement LaidCardsToXml()
        {
            XElement xElement = new XElement(LaidCardsElementName);

            foreach (Card card in LaidCards)
            {
                xElement.Add(new XElement(card.ToXml()));
            }

            return xElement;
        }

        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(Table);

            XElement xElement = new XElement(name);

            xElement.Add(new XAttribute(IdAttributeName, Id));
            xElement.Add(new XAttribute(CreatedAttributeName, Created.ToString()));
            xElement.Add(new XAttribute(LastPlayedAttributeName, DateTime.Now.ToString()));
            
            xElement.Add(_deckOfCards.ToXml(DeckOfCardsElementName));
            xElement.Add(LaidCardsToXml());
            xElement.Add(_movableCards.ToXml(MovableCardsElementName));

            return xElement;
        }

        public Table(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidAttributes(xElementContainer.XElement,
                                               new[] {IdAttributeName, CreatedAttributeName, LastPlayedAttributeName},
                                               out exception))
                throw exception;
            if (!XmlHandler.HasValidElements(xElementContainer.XElement,
                                             new[] {DeckOfCardsElementName, LaidCardsElementName, MovableCardsElementName},
                                             out exception))
                throw exception;

            string idString = xElementContainer.XElement.Attribute(IdAttributeName).Value;
            Guid id;
            if (!Guid.TryParse(idString, out id))
                throw new ArgumentException($"{nameof(idString)} is not a valid string.");
            Id = id;

            string createdString = xElementContainer.XElement.Attribute(CreatedAttributeName).Value;
            DateTime created;
            if (!DateTime.TryParse(createdString, out created))
                throw new ArgumentException($"{nameof(createdString)} is not a valid string.");
            Created = created;

            string lastPlayedString = xElementContainer.XElement.Attribute(LastPlayedAttributeName).Value;
            DateTime lastPlayed;
            if (!DateTime.TryParse(lastPlayedString, out lastPlayed))
                throw new ArgumentException($"{nameof(lastPlayedString)} is not a valid string.");
            LastPlayed = lastPlayed;

            _deckOfCards = new CardCollection(ToListOfCards(xElementContainer.XElement.Element(DeckOfCardsElementName)));
            try
            {
                List<Card> listOfCards = ToListOfCards(xElementContainer.XElement.Element(LaidCardsElementName));
                if (listOfCards.Count != 0)
                    new CardCollection(listOfCards);
                _laidCards = listOfCards;
            }
            catch (Exception e)
            {
                throw e;
            }
            _movableCards = new MovableCardsCollection(ToListOfCardCardPositionsPair(xElementContainer.XElement.Element(MovableCardsElementName)));
        }

        private List<Card> ToListOfCards(XElement xElement)
        {
            if (xElement == null)
                throw new ArgumentNullException($"{nameof(xElement)} cannot be null.");
            if (xElement.Elements().Count() > CardCollection.MaxCapacity)
                throw new ArgumentException($"{nameof(xElement)} cannot contain more than {CardCollection.MaxCapacity} elements.");

            List<Card> listOfCards = new List<Card>();
            foreach (XElement element in xElement.Elements())
            {
                Card card = new Card(new XElementContainer(element));
                listOfCards.Add(card);
            }

            return listOfCards;
        }

        private List<CardCardPositionsPair> ToListOfCardCardPositionsPair(XElement xElement)
        {
            if (xElement == null)
                throw new ArgumentNullException($"{nameof(xElement)} cannot be null.");
            if (xElement.Elements().Count() > MovableCardsCollection.MaxCapacity)
                throw new ArgumentException($"{nameof(xElement)} cannot contain more than {MovableCardsCollection.MaxCapacity} elements.");

            List<CardCardPositionsPair> listOfCardCardPositionsPair = new List<CardCardPositionsPair>();
            foreach (XElement element in xElement.Elements())
            {
                CardCardPositionsPair ccpp = new CardCardPositionsPair(new XElementContainer(element));
                listOfCardCardPositionsPair.Add(ccpp);
            }

            return listOfCardCardPositionsPair;
        }
    }
}