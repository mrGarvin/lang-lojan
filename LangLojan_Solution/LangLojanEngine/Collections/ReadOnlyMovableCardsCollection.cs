﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Collections
{
    public class ReadOnlyMovableCardsCollection : IReadOnlyMovableCardsCollection
    {
        public CardCardPositionsPair this[int index] => _movableCards[index];
        public int MaxCapacity { get; }
        public int Count { get; }
        public bool IsEmpty { get; }
        public bool IsFull { get; }
        
        private readonly List<CardCardPositionsPair> _movableCards;

        public ReadOnlyMovableCardsCollection(MovableCardsCollection movableCardsCollection)
        {
            if (movableCardsCollection == null)
                throw new ArgumentNullException($"{nameof(movableCardsCollection)} cannot be null.");

            MaxCapacity = MovableCardsCollection.MaxCapacity;
            Count = movableCardsCollection.Count;
            IsEmpty = movableCardsCollection.IsEmpty;
            IsFull = movableCardsCollection.IsFull;
            _movableCards = new List<CardCardPositionsPair>(MaxCapacity);
            _movableCards.AddRange(movableCardsCollection);
        }
        
        public bool Contains(Card card)
        {
            return card != null && _movableCards.Any(ccpp => ccpp.Card.Equals(card));
        }

        public bool Contains(CardPositions cardPositions)
        {
            return cardPositions.IsValid() && _movableCards.Any(ccpp => ccpp.CardPositions.Equals(cardPositions));
        }

        public bool Contains(uint position)
        {
            return
                _movableCards.Any(
                    ccpp =>
                        ccpp.CardPositions.OneStepBack != null && ccpp.CardPositions.OneStepBack.Value == position ||
                        ccpp.CardPositions.ThreeStepsBack != null && ccpp.CardPositions.ThreeStepsBack.Value == position);
        }

        public Card[] GetAllCards()
        {
            return _movableCards.Select(ccpp => ccpp.Card).ToArray();
        }

        public CardPositions[] GetAllCardPositions()
        {
            return _movableCards.Select(ccpp => ccpp.CardPositions).ToArray();
        }

        public CardPositions GetCardPositionsBy(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            var kvpCollection = _movableCards.Where(ccpp => Equals(ccpp.Card, card));
            if (!kvpCollection.Any())
                throw new ArgumentException($"The {nameof(CardPositions)} associated with the {nameof(Card)} '{card}' does not exist in the collection.");

            return kvpCollection.First().CardPositions;
        }

        public int IndexOf(Card card)
        {
            CardPositions cardPositions = GetCardPositionsBy(card);
            return _movableCards.IndexOf(new CardCardPositionsPair(card, cardPositions));
        }

        public MovableCardsCollection ToMovableCardsCollection()
        {
            return new MovableCardsCollection(_movableCards);
        }

        public string[] ToStringArray()
        {
            List<string> listOfStrings = new List<string>();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                listOfStrings.Add(ccpp.Card.ToString());
            }
            return listOfStrings.ToArray();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                stringBuilder.Append(ccpp.Card);
                stringBuilder.Append(": ");
                stringBuilder.Append(ccpp.CardPositions);
                stringBuilder.Append(ccpp.Card.Equals(_movableCards.Last().Card) ? "" : ", ");
            }
            return stringBuilder.ToString();
        }

        public string ToString(bool newLineAsCardSeparator)
        {
            if (!newLineAsCardSeparator)
                return ToString();

            StringBuilder stringBuilder = new StringBuilder();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                stringBuilder.AppendLine(ccpp.ToString());
            }
            return stringBuilder.ToString();
        }

        public IEnumerator<CardCardPositionsPair> GetEnumerator()
        {
            return _movableCards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}