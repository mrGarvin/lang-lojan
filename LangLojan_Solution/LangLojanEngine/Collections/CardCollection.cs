﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Resources;
using System.Text;
using System.Xml.Linq;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Collections
{
    public class CardCollection : ICardCollection
    {
        public static int MaxCapacity => 52;
        public static int NumberOfSuits => Enum.GetValues(typeof(CardSuit)).Length;
        public static int MinNumberOfDenominations => 1;
        public static int MaxNumberOfDenominations => Enum.GetValues(typeof(CardDenomination)).Length;

        private static readonly Random random = new Random();

        public Card this[int index] => _cards[index];
        public int Count => _cards.Count;
        public bool IsShuffled { get; private set; }
        public bool IsEmpty => Count == 0;
        public bool IsFull => Count == MaxCapacity;
        private readonly List<Card> _cards = new List<Card>(MaxCapacity);

        /// <summary>
        /// Creates a CardCollection with 52 cards and shuffles it.
        /// </summary>
        public CardCollection()
        {
            List<Card> listOfCards = new List<Card>(MaxCapacity);
            foreach (CardSuit cardSuit in Enum.GetValues(typeof(CardSuit)))
            {
                foreach (CardDenomination cardDenomination in Enum.GetValues(typeof(CardDenomination)))
                {
                    listOfCards.Add(new Card(cardDenomination, cardSuit));
                }
            }

            _cards = listOfCards;
            Shuffle();
        }

        /// <summary>
        /// Creates a CardCollection with n cards in each suit.
        /// </summary>
        /// <param name="numberOfCardsInEachSuit"></param>
        /// <param name="shuffle"></param>
        public CardCollection(uint numberOfCardsInEachSuit, bool shuffle)
        {
            if (numberOfCardsInEachSuit == 0)
                throw new ArgumentOutOfRangeException($"{nameof(numberOfCardsInEachSuit)} cannot be 0.");
            if (numberOfCardsInEachSuit > Enum.GetValues(typeof(CardDenomination)).Length)
                throw new ArgumentOutOfRangeException(
                    $"{nameof(numberOfCardsInEachSuit)} cannot be larger than {Enum.GetValues(typeof(CardDenomination)).Length}.");

            List<Card> listOfCards =
                new List<Card>(Enum.GetValues(typeof(CardSuit)).Length * (int)numberOfCardsInEachSuit);
            foreach (CardSuit cardSuit in Enum.GetValues(typeof(CardSuit)))
            {
                for (int i = 0; i < numberOfCardsInEachSuit; i++)
                {
                    listOfCards.Add(new Card((CardDenomination)Enum.GetValues(typeof(CardDenomination)).GetValue(i),
                        cardSuit));
                }
            }

            _cards = listOfCards;

            if (shuffle)
                Shuffle();
        }

        /// <summary>
        /// Creates a CardCollection from a collection of cards.
        /// </summary>
        /// <param name="cards"></param>
        /// <param name="shuffle"></param>
        public CardCollection(IEnumerable<Card> cards, bool shuffle)
        {
            AddToTop(cards);

            if (shuffle)
                Shuffle();
        }

        public CardCollection(IEnumerable<Card> cards)
        {
            AddToTop(cards);
            IsShuffled = CollectionIsShuffled();
        }



        private void Shuffle()
        {
            List<Card> tmp = _cards.Clone().ToList();
            _cards.Clear();
            while (tmp.Count != 0)
            {
                int randomCardPosition = random.Next(tmp.Count);
                _cards.Add(tmp[randomCardPosition]);
                tmp.RemoveAt(randomCardPosition);
            }

            IsShuffled = true;
        }

        public Card TakeFirst()
        {
            if (_cards.Count == 0)
                return null;

            Card firstCard = _cards.First();
            _cards.RemoveAt(0);
            return firstCard;
        }

        public void AddToTop(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            if (_cards.Contains(card))
                throw new ArgumentException($"The {nameof(Card)} '{card}' already exist in the collection.");
            if (IsFull)
                throw new ArgumentException(
                    $"The {nameof(CardCollection)} is full. It can only contain '{MaxCapacity}' cards.");

            _cards.Insert(0, card);
        }

        public void AddToTop(IEnumerable<Card> cards)
        {
            if (cards == null)
                throw new ArgumentNullException($"{nameof(cards)} cannot be null.");
            if (!cards.Any())
                throw new ArgumentException($"{nameof(cards)} cannot be empty.");
            if (cards.Count() > MaxCapacity)
                throw new ArgumentException($"{nameof(cards)} cannot contain more than {MaxCapacity} cards.");

            foreach (Card card in cards.Reverse())
            {
                AddToTop(card);
            }
        }

        private bool CollectionIsShuffled()
        {
            return !(CollectionContainsTheSameNumberOfSuits() &&
                     CollectionContainsTheSameNumberOfDenominations() &&
                     !CardsInCollectionAreInShuffledOrder());
        }

        private bool CollectionContainsTheSameNumberOfSuits()
        {
            if (_cards.Count == 0)
                return true;
            if (_cards.Count % 4 != 0)
                return false;
            var nHearts = _cards.Count(c => c.Suit == CardSuit.Hearts);
            return nHearts == _cards.Count(c => c.Suit == CardSuit.Spades) &&
                   nHearts == _cards.Count(c => c.Suit == CardSuit.Diamonds) &&
                   nHearts == _cards.Count(c => c.Suit == CardSuit.Clubs);
        }

        private bool CollectionContainsTheSameNumberOfDenominations()
        {
            if (_cards.Count == 0)
                return true;
            if (_cards.Count % 4 != 0)
                return false;
            var cardArray = _cards.Where(c => c.Suit == CardSuit.Hearts) as Card[] ?? _cards.Where(c => c.Suit == CardSuit.Hearts).ToArray();
            return cardArray.Any() && cardArray.All(card => _cards.Count(c => c.Denomination == card.Denomination) == 4);
        }

        private bool CardsInCollectionAreInShuffledOrder()
        {
            if (_cards.Count == 0)
                return false;
            if (_cards.Count % 4 != 0)
                return true;
            var expectedNumberOfCardsInEachSuit = _cards.Count / 4;
            int startPos = 0,
                endPos = 3;
            for (int i = 0; i < 4; i++)
            {
                var tmpCards = new List<Card>();
                for (int j = startPos; j < endPos; j++)
                {
                    tmpCards.Add(_cards[j]);
                }
                for (int k = 0; k < expectedNumberOfCardsInEachSuit; k++)
                {
                    if ((int)tmpCards[k].Suit != i || (int)tmpCards[k].Denomination != k + 2)
                        return true;
                }
                startPos += expectedNumberOfCardsInEachSuit;
                endPos += expectedNumberOfCardsInEachSuit;
            }

            return false;
        }

        public IEnumerator<Card> GetEnumerator()
        {

            return _cards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Card card in _cards)
            {
                stringBuilder.Append(card);
                stringBuilder.Append(card.Equals(_cards.Last()) ? "" : ", ");
            }
            return stringBuilder.ToString();
        }

        public string ToString(bool newLineAsCardSeparator)
        {
            if (!newLineAsCardSeparator)
                return ToString();

            StringBuilder stringBuilder = new StringBuilder();
            foreach (Card card in _cards)
            {
                stringBuilder.AppendLine(card.ToString());
            }
            return stringBuilder.ToString();
        }

        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(CardCollection);

            XElement xElement = new XElement(name);
            foreach (Card card in _cards)
            {
                xElement.Add(card.ToXml());
            }
            xElement.Add(new XAttribute(nameof(IsShuffled), IsShuffled));

            return xElement;
        }

        public CardCollection(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidAttributes(xElementContainer.XElement,
                                               new[] {nameof(IsShuffled)}, 
                                               out exception))
                throw exception;
            if (!XmlHandler.HasValidNumberOfElements(xElementContainer.XElement, 
                                                     new XmlHandler.ValueRange(1, (uint) MaxCapacity), 
                                                     out exception))
                throw exception;

            string isShuffledString = xElementContainer.XElement.Attribute(nameof(IsShuffled)).Value;
            bool isShuffled;
            if (!bool.TryParse(isShuffledString, out isShuffled))
                throw new ArgumentException($"{nameof(isShuffledString)} is not a valid string.");
            IsShuffled = isShuffled;

            List<Card> cards = new List<Card>();
            foreach (XElement xElement in xElementContainer.XElement.Elements())
            {
                Card card = new Card(new XElementContainer(xElement));
                cards.Add(card);
            }

            _cards = new CardCollection(cards)._cards;
        }
    }
}
