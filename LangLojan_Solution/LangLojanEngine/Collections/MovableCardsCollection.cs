﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace LangLojanEngine.Collections
{
    public class MovableCardsCollection : IMovableCardsCollection
    {
        public static int MaxCapacity => 52;
        
        public CardCardPositionsPair this[int index] => _movableCards[index];
        public int Count => _movableCards.Count;
        public bool IsEmpty => Count == 0;
        public bool IsFull => Count == MaxCapacity;

        private readonly List<CardCardPositionsPair> _movableCards = new List<CardCardPositionsPair>(MaxCapacity);

        public MovableCardsCollection() { }

        public MovableCardsCollection(IEnumerable<CardCardPositionsPair> movableCards)
        {
            if (movableCards == null)
                throw new ArgumentNullException($"{nameof(movableCards)} cannot be null.");
            if (movableCards.Count() > MaxCapacity)
                throw new ArgumentException($"{nameof(movableCards)} cannot contain more than {MaxCapacity} {nameof(CardCardPositionsPair)}'s.");

            foreach (CardCardPositionsPair ccpp in movableCards)
            {
                Add(ccpp.Card, ccpp.CardPositions);
            }
        }

        public void Add(Card card, CardPositions cardPositions)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            if (!cardPositions.IsValid())
                throw new ArgumentException($"{nameof(cardPositions)} is not a valid {nameof(CardPositions)}.");
            if (_movableCards.Count(ccpp => Equals(ccpp.Card, card)) != 0)
                throw new ArgumentException($"The {nameof(Card)} '{card}' already exist in the collection.");
            if (IsFull)
                throw new ArgumentException(
                    $"The {nameof(MovableCardsCollection)} is full. It can only contain '{MaxCapacity}' cards.");

            _movableCards.Add(new CardCardPositionsPair(card, cardPositions));
        }

        public bool Contains(Card card)
        {
            return card != null && _movableCards.Any(ccpp => ccpp.Card.Equals(card));
        }

        public bool Contains(CardPositions cardPositions)
        {
            return cardPositions.IsValid() && _movableCards.Any(ccpp => ccpp.CardPositions.Equals(cardPositions));
        }

        public bool Contains(uint position)
        {
            return
                _movableCards.Any(
                    ccpp =>
                        ccpp.CardPositions.OneStepBack != null && ccpp.CardPositions.OneStepBack.Value == position ||
                        ccpp.CardPositions.ThreeStepsBack != null && ccpp.CardPositions.ThreeStepsBack.Value == position);
        }

        public Card[] GetAllCards()
        {
            return _movableCards.Select(ccpp => ccpp.Card).ToArray();
        }

        public CardPositions[] GetAllCardPositions()
        {
            return _movableCards.Select(ccpp => ccpp.CardPositions).ToArray();
        }

        public CardPositions GetCardPositionsBy(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            var kvpCollection = _movableCards.Where(ccpp => Equals(ccpp.Card, card));
            if (!kvpCollection.Any())
                throw new ArgumentException($"The {nameof(CardPositions)} associated with the {nameof(Card)} '{card}' does not exist in the collection.");

            return kvpCollection.First().CardPositions;
        }

        public int IndexOf(Card card)
        {
            CardPositions cardPositions = GetCardPositionsBy(card);
            return _movableCards.IndexOf(new CardCardPositionsPair(card, cardPositions));
        }

        public void Remove(Card card)
        {
            if (card == null)
                throw new ArgumentNullException($"{nameof(card)} cannot be null.");
            var kvpCollection = _movableCards.Where(ccpp => Equals(ccpp.Card, card));
            if (!kvpCollection.Any())
                throw new ArgumentException($"The {nameof(Card)} '{card}' does not exist in the collection.");

            _movableCards.Remove(kvpCollection.First());
        }

        public void RemoveCardAt(int index)
        {
            if (0 > index || index >= _movableCards.Count)
                throw new ArgumentOutOfRangeException($"{nameof(index)} must be between 0 and {_movableCards.Count}.");
            if (IsEmpty)
                throw new ArgumentException("The collection is empty.");

            _movableCards.RemoveAt(index);
        }

        public void Clear()
        {
            _movableCards.Clear();
        }

        public string[] ToStringArray()
        {
            List<string> listOfStrings = new List<string>();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                listOfStrings.Add(ccpp.Card.ToString());
            }
            return listOfStrings.ToArray();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                stringBuilder.Append(ccpp.Card);
                stringBuilder.Append(": ");
                stringBuilder.Append(ccpp.CardPositions);
                stringBuilder.Append(ccpp.Card.Equals(_movableCards.Last().Card) ? "" : ", ");
            }
            return stringBuilder.ToString();
        }

        public string ToString(bool newLineAsCardSeparator)
        {
            if (!newLineAsCardSeparator)
                return ToString();

            StringBuilder stringBuilder = new StringBuilder();
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                stringBuilder.AppendLine(ccpp.ToString());
            }
            return stringBuilder.ToString();
        }

        public IEnumerator<CardCardPositionsPair> GetEnumerator()
        {
            return _movableCards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public XElement ToXml(string name = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = nameof(MovableCardsCollection);

            XElement xElement = new XElement(name);
            foreach (CardCardPositionsPair ccpp in _movableCards)
            {
                xElement.Add(ccpp.ToXml());
            }

            return xElement;
        }

        public MovableCardsCollection(XElementContainer xElementContainer)
        {
            Exception exception;
            if (!XmlHandler.HasValidNumberOfElements(xElementContainer.XElement,
                                                     new XmlHandler.ValueRange(0, (uint) MaxCapacity), 
                                                     out exception))
                throw exception;

            List<CardCardPositionsPair> movableCards = new List<CardCardPositionsPair>();
            foreach (XElement xElement in xElementContainer.XElement.Elements())
            {
                CardCardPositionsPair ccpp = new CardCardPositionsPair(new XElementContainer(xElement));
                movableCards.Add(ccpp);
            }

            _movableCards = new MovableCardsCollection(movableCards)._movableCards;
        }
    }
}