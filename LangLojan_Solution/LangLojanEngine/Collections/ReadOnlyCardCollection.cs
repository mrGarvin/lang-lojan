﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;

namespace LangLojanEngine.Collections
{
    public class ReadOnlyCardCollection : IReadOnlyCardCollection
    {
        public int MaxCapacity { get; }
        public int NumberOfSuits { get; }
        public int MinNumberOfDenominations { get; }
        public int MaxNumberOfDenominations { get; }

        public Card this[int index] => _cards[index];
        public int Count { get; }
        public bool IsShuffled { get; }
        public bool IsEmpty { get; }
        public bool IsFull { get; }

        private readonly List<Card> _cards;

        public ReadOnlyCardCollection(CardCollection cardCollection)
        {
            if (cardCollection == null)
                throw new ArgumentNullException($"{nameof(cardCollection)} cannot be null.");

            Count = cardCollection.Count;
            IsShuffled = cardCollection.IsShuffled;
            IsEmpty = cardCollection.IsEmpty;
            IsFull = cardCollection.IsFull;
            MaxCapacity = CardCollection.MaxCapacity;
            NumberOfSuits = CardCollection.NumberOfSuits;
            MinNumberOfDenominations = CardCollection.MinNumberOfDenominations;
            MaxNumberOfDenominations = CardCollection.MaxNumberOfDenominations;
            _cards = new List<Card>(MaxCapacity);
            _cards.AddRange(cardCollection);
        }

        public CardCollection ToCardCollection()
        {
            return new CardCollection(_cards);
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Card card in _cards)
            {
                stringBuilder.Append(card);
                stringBuilder.Append(card.Equals(_cards.Last()) ? "" : ", ");
            }
            return stringBuilder.ToString();
        }

        public string ToString(bool newLineAsCardSeparator)
        {
            if (!newLineAsCardSeparator)
                return ToString();

            StringBuilder stringBuilder = new StringBuilder();
            foreach (Card card in _cards)
            {
                stringBuilder.AppendLine(card.ToString());
            }
            return stringBuilder.ToString();
        }

        public IEnumerator<Card> GetEnumerator()
        {
            return _cards.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}