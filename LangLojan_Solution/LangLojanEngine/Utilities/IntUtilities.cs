﻿using System.Linq;

namespace LangLojanEngine.Utilities
{
    public static class IntUtilities
    {
        /// <summary>
        /// Reports the zero-based index of the first occurrence in this instance of any character in a specified array of Unicode characters.
        /// </summary>
        /// <returns>
        /// The zero-based index position of the first occurrence in this instance where any character in anyOf was found; -1 if no character in anyOf was found.
        /// </returns>
        /// <param name="str"></param>
        /// <param name="anyOf"></param>
        /// <param name="ignore"></param>
        /// <returns></returns>
        public static int IndexOfAny(this string str, char[] anyOf, char[] ignore)
        {
            foreach (char c in str)
            {
                if (c != '\\' && ignore.Contains(c))
                    return str.IndexOf(c);
            }

            return -1;
        }
    }
}