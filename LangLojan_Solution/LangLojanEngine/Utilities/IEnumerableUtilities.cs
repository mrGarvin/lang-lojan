﻿using System;
using System.Collections.Generic;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Utilities
{
    public static class IEnumerableUtilities
    {
        public static IEnumerable<T> Clone<T>(this IEnumerable<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException($"{nameof(collection)} cannot be null.");

            List<T> list = new List<T>();
            foreach (T t in collection)
            {
                list.Add(t);
            }

            return list;
        }
    }
}