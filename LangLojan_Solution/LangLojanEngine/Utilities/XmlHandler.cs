﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;

namespace LangLojanEngine.Utilities
{
    public static class XmlHandler
    {
        #region TryLoadFrom
        public static bool TryLoadFrom(string filePath, out CardCollection cardCollection, out Exception exception)
        {
            try
            {
                XDocument xDocument = XDocument.Load(filePath);
                cardCollection = new CardCollection(new XElementContainer(xDocument.Root));
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                cardCollection = null;
                exception = e;
                return false;
            }
        }

        public static bool TryLoadFrom(string filePath, out Table table, out Exception exception)
        {
            try
            {
                XDocument xDocument = XDocument.Load(filePath);
                table = new Table(new XElementContainer(xDocument.Root));
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                table = null;
                exception = e;
                return false;
            }
        }
        #endregion TryLoadFrom

        #region TrySaveTo
        public static bool TrySaveTo(string filePath, IXmlItem xmlItem, out Exception exception)
        {
            if (xmlItem == null)
            {
                exception = new ArgumentNullException($"{nameof(xmlItem)} cannot be null.");
                return false;
            }
            XDocument xDocument = new XDocument(xmlItem.ToXml());
            try
            {
                xDocument.Save(filePath);
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }
        #endregion TrySaveTo

        #region Validation of attributes
        public static bool HasValidAttributes(XElement xElement, string[] attributeNames, out Exception exception)
        {
            if (xElement == null)
            {
                exception = new ArgumentNullException($"The {nameof(xElement)} cannot be null.");
                return false;
            }
            if (attributeNames == null)
            {
                exception = new ArgumentNullException($"{nameof(attributeNames)} cannot be null.");
                return false;
            }
            if (attributeNames.Length == 0)
            {
                exception = new ArgumentException($"{nameof(attributeNames)} must contain at least 1 attribute name.");
                return false;
            }
            if (!xElement.HasAttributes)
            {
                exception = new ArgumentException($"The {nameof(xElement)} must have {attributeNames.Length} attributes.");
                return false;
            }
            if (xElement.Attributes().Count() > attributeNames.Length)
            {
                exception = new ArgumentException($"The {nameof(xElement)} cannot have more than {attributeNames.Length} attributes.");
                return false;
            }
            foreach (string attributeName in attributeNames)
            {
                if (xElement.Attribute(attributeName) != null)
                    continue;
                exception = new ArgumentException($"The {nameof(xElement)} is missing the attribute {attributeName}.");
                return false;
            }

            exception = null;
            return true;
        }
        #endregion Validation of attributes

        #region Validation of elements
        public static bool HasValidElements(XElement xElement, string[] elementNames, out Exception exception)
        {
            if (xElement == null)
            {
                exception = new ArgumentNullException($"The {nameof(xElement)} cannot be null.");
                return false;
            }
            if (elementNames == null)
            {
                exception = new ArgumentNullException($"{nameof(elementNames)} cannot be null.");
                return false;
            }
            if (elementNames.Length == 0)
            {
                exception = new ArgumentException($"{nameof(elementNames)} must contain at least 1 element name.");
                return false;
            }
            if (!xElement.HasElements)
            {
                exception = new ArgumentException($"The {nameof(xElement)} must have {elementNames.Length} elements.");
                return false;
            }
            if (xElement.Elements().Count() > elementNames.Length)
            {
                exception = new ArgumentException($"The {nameof(xElement)} cannot have more than {elementNames.Length} elements.");
                return false;
            }
            foreach (string elementName in elementNames)
            {
                if (xElement.Element(elementName) != null)
                    continue;
                exception = new ArgumentException($"The {nameof(xElement)} is missing the element {elementName}.");
                return false;
            }

            exception = null;
            return true;
        }

        public static bool HasValidNumberOfElements(XElement xElement, ValueRange valueRange, out Exception exception)
        {
            if (xElement == null)
            {
                exception = new ArgumentNullException($"The {nameof(xElement)} cannot be null.");
                return false;
            }
            if (xElement.Elements().Count() < valueRange.MinValue)
            {
                exception = new ArgumentException($"The {nameof(xElement)} cannot have less than {valueRange.MinValue} elements.");
                return false;
            }
            if (xElement.Attributes().Count() > valueRange.MaxValue)
            {
                exception = new ArgumentException($"The {nameof(xElement)} cannot have more than {valueRange.MaxValue} elements.");
                return false;
            }

            exception = null;
            return true;
        }
        #endregion Validation of elements

        #region Container objects
        public struct ValueRange
        {
            public uint MinValue { get; }
            public uint MaxValue { get; }

            public ValueRange(uint minValue, uint maxValue)
            {
                if (minValue > maxValue)
                    throw new ArgumentException($"{nameof(minValue)} cannot be larger than {nameof(maxValue)}");

                MinValue = minValue;
                MaxValue = maxValue;
            }
        }
        #endregion Container objects
    }
}