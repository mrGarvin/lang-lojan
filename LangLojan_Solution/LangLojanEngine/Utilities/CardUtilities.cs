﻿using System;
using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Utilities
{
    public static class CardUtilities
    {
        public static bool Match(this Card card, Card otherCard)
        {
            if (otherCard == null)
                return false;

            return card.Denomination == otherCard.Denomination || card.Suit == otherCard.Suit;
        }

        public static string[] ToStringArray(this Card[] cardArray)
        {
            string[] s = new string[cardArray.Length];
            for (int i = 0; i < cardArray.Length; i++)
            {
                s[i] = cardArray[i].ToString();
            }
            return s;
        }
    }
}