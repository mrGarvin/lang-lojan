﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LangLojanEngine.Utilities
{
    public static class TryGetPath
    {
        #region enums
        public enum BasePath
        {
            FromAppDomain,
            FromDirectory,
            FromReflection
        }

        private enum StartDirectory
        {
            BasePath,
            Project,
            Solution
        }

        private enum Item
        {
            Directory,
            File
        }
        #endregion enums

        #region ToDirectory

        /// <summary>
        /// Tries to create a path to a directory. Option: Tries to create the directory if it does not exist.
        /// </summary>
        /// <param name="directoryName"></param>
        /// <param name="createDirectoryIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToDirectory(string directoryName, 
                                       bool createDirectoryIfItDoesNotExist,
                                       out string path, out Exception exception,
                                       BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(directoryName, basePath, 
                          StartDirectory.BasePath, Item.Directory,
                          createDirectoryIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToDirectory(string[] directoryNames, 
                                       bool createDirectoryIfItDoesNotExist, 
                                       out string path, out Exception exception,
                                       BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(directoryNames, out exception))
            {
                path = null;
                return false;
            }

            if (ToDirectory(Path.Combine(directoryNames), 
                            createDirectoryIfItDoesNotExist,
                            out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToDirectory

        #region ToDirectoryInProject
        /// <summary>
        /// Tries to create a path to a directory. Option: Tries to create the directory if it does not exist.
        /// </summary>
        /// <param name="directoryName"></param>
        /// <param name="createDirectoryIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToDirectoryInProject(string directoryName, 
                                                bool createDirectoryIfItDoesNotExist,
                                                out string path, out Exception exception,
                                                BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(directoryName, basePath, 
                          StartDirectory.Project, Item.Directory,
                          createDirectoryIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToDirectoryInProject(string[] directoryNames, 
                                                bool createDirectoryIfItDoesNotExist,
                                                out string path, out Exception exception,
                                                BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(directoryNames, out exception))
            {
                path = null;
                return false;
            }

            if (ToDirectoryInProject(Path.Combine(directoryNames), 
                                     createDirectoryIfItDoesNotExist,
                                     out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToDirectoryInProject

        #region ToDirectoryInSolution
        /// <summary>
        /// Tries to create a path to a directory. Option: Tries to create the directory if it does not exist.
        /// </summary>
        /// <param name="directoryName"></param>
        /// <param name="createDirectoryIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToDirectoryInSolution(string directoryName, 
                                                 bool createDirectoryIfItDoesNotExist,
                                                 out string path, out Exception exception,
                                                 BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(directoryName, basePath, 
                          StartDirectory.Solution, Item.Directory,
                          createDirectoryIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToDirectoryInSolution(string[] directoryNames, 
                                                 bool createDirectoryIfItDoesNotExist,
                                                 out string path, out Exception exception,
                                                 BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(directoryNames, out exception))
            {
                path = null;
                return false;
            }

            if (ToDirectoryInSolution(Path.Combine(directoryNames), 
                                      createDirectoryIfItDoesNotExist,
                                      out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToDirectoryInSolution

        #region ToFile

        /// <summary>
        /// Tries to create a path to a file. Option: Tries to create the file if it does not exist.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="createFileIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToFile(string fileName, 
                                  bool createFileIfItDoesNotExist,
                                  out string path, out Exception exception,
                                  BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(fileName, basePath, 
                          StartDirectory.BasePath, Item.File, 
                          createFileIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToFile(string fileName, string[] subdirectories,
                                  bool createFileIfItDoesNotExist, 
                                  out string path, out Exception exception,
                                  BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(fileName, out exception) || IsNotValid(subdirectories, out exception))
            {
                path = null;
                return false;
            }

            if (ToFile(Path.Combine(Path.Combine(subdirectories), fileName), 
                       createFileIfItDoesNotExist,
                       out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToFile

        #region ToFileInProject
        /// <summary>
        /// Tries to create a path to a file. Option: Tries to create the file if it does not exist.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="createFileIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToFileInProject(string fileName, 
                                           bool createFileIfItDoesNotExist,
                                           out string path, out Exception exception,
                                           BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(fileName, basePath, 
                          StartDirectory.Project, Item.File, 
                          createFileIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToFileInProject(string fileName, string[] subdirectories,
                                           bool createFileIfItDoesNotExist, 
                                           out string path, out Exception exception,
                                           BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(fileName, out exception) || IsNotValid(subdirectories, out exception))
            {
                path = null;
                return false;
            }

            if (ToFileInProject(Path.Combine(Path.Combine(subdirectories), fileName), 
                                createFileIfItDoesNotExist,
                                out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToFileInProject

        #region ToFileInSolution
        /// <summary>
        /// Tries to create a path to a file. Option: Tries to create the file if it does not exist.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="createFileIfItDoesNotExist"></param>
        /// <param name="path"></param>
        /// <param name="exception"></param>
        /// <param name="basePath"></param>
        /// <returns></returns>
        public static bool ToFileInSolution(string fileName, 
                                            bool createFileIfItDoesNotExist,
                                            out string path, out Exception exception,
                                            BasePath basePath = BasePath.FromAppDomain)
        {
            return ToItem(fileName, basePath, 
                          StartDirectory.Solution, Item.File, 
                          createFileIfItDoesNotExist, 
                          out path, out exception);
        }

        public static bool ToFileInSolution(string fileName, string[] subdirectories, 
                                            bool createFileIfItDoesNotExist,
                                            out string path, out Exception exception,
                                            BasePath basePath = BasePath.FromAppDomain)
        {
            if (IsNotValid(fileName, out exception) || IsNotValid(subdirectories, out exception))
            {
                path = null;
                return false;
            }

            if (ToFileInSolution(Path.Combine(Path.Combine(subdirectories), fileName), 
                                 createFileIfItDoesNotExist,
                                 out path, out exception, basePath))
                return true;

            path = null;
            return false;
        }
        #endregion ToFileInSolution
        
        #region Private methods
        #region Validation
        private static bool IsNotValid(string itemName, out Exception exception)
        {
            if (itemName == null)
            {
                exception =
                    new ArgumentNullException(
                        $"{nameof(itemName)} is null.");
                return true;
            }
            if (string.IsNullOrWhiteSpace(itemName))
            {
                exception =
                    new ArgumentException(
                        $"{nameof(itemName)} is either empty or consists only of white-space characters.");
                return true;
            }
            int invalidFileNameCharPos = itemName.IndexOfAny(Path.GetInvalidFileNameChars(), new []{'\\'});
            if (invalidFileNameCharPos >= 0)
            {
                exception =
                    new ArgumentException(
                        $"{nameof(itemName)} contains '{itemName[invalidFileNameCharPos]}' which is an invalid file/directory name character.");
                return true;
            }

            exception = null;
            return false;
        }

        private static bool IsNotValid(string[] itemNames, out Exception exception)
        {
            if (itemNames == null)
            {
                exception =
                    new ArgumentNullException(
                        $"{nameof(itemNames)} is null.");
                return true;
            }
            if (itemNames.Length == 0)
            {
                exception =
                    new ArgumentException(
                        $"{nameof(itemNames)} is empty.");
                return true;
            }
            foreach (string itemName in itemNames)
            {
                if (IsNotValid(itemName, out exception))
                    return true;
            }

            exception = null;
            return false;
        }
        #endregion Validation

        private static bool ToItem(string name, BasePath basePath, StartDirectory startDirectory,
            Item item, bool createItemIfItDoesNotExist, out string path, out Exception exception)
        {
            if (!TryCreatePath(name, basePath, startDirectory, out path, out exception))
                return false;
            if (!ItemExists(item, path))
            {
                if (!createItemIfItDoesNotExist)
                    return true;

                return TryCreateItem(ref path, item, out exception);
            }

            exception = null;
            return true;
        }

        private static bool ItemExists(Item item, string path)
        {
            switch (item)
            {
                case Item.Directory:
                    return Directory.Exists(path);
                case Item.File:
                    return File.Exists(path);
                default:
                    throw new ArgumentOutOfRangeException(nameof(item), item, null);
            }
        }

        private static bool TryCreatePath(string itemName,
            BasePath basePath, StartDirectory startDirectory,
            out string path, out Exception exception)
        {

            if (IsNotValid(itemName, out exception))
            {
                path = null;
                return false;
            }

            if (!TryGetBasePath(basePath, out path, out exception))
                return false;

            int numberOfParents;
            switch (startDirectory)
            {
                case StartDirectory.BasePath:
                    numberOfParents = 0;
                    break;
                case StartDirectory.Project:
                    numberOfParents = 3;
                    break;
                case StartDirectory.Solution:
                    numberOfParents = 4;
                    break;
                default:
                    exception = new ArgumentOutOfRangeException(nameof(startDirectory), startDirectory, null);
                    return false;
            }

            if (numberOfParents > 0)
                if (!TryGetDirectoryParent(path, numberOfParents,
                    out path, out exception))
                    return false;

            try
            {
                path = Path.Combine(path, itemName);
                Path.GetFullPath(path);
            }
            catch (PathTooLongException e)
            {
                exception = e;
                path = null;
                return false;
            }

            exception = null;
            return true;
        }

        private static bool TryGetBasePath(BasePath basePath, out string path, out Exception exception)
        {
            switch (basePath)
            {
                case BasePath.FromAppDomain:
                    try
                    {
                        path = AppDomain.CurrentDomain.BaseDirectory;
                        if (path[path.Length - 1] != '\\')
                            path += "\\\\";

                        exception = null;
                        return true;
                    }
                    catch (Exception e)
                    {
                        path = null;
                        exception = e;
                        return false;
                    }
                case BasePath.FromDirectory:
                    try
                    {
                        path = Directory.GetCurrentDirectory();
                        if (path[path.Length - 1] != '\\')
                            path += "\\\\";

                        exception = null;
                        return true;
                    }
                    catch (Exception e)
                    {
                        path = null;
                        exception = e;
                        return false;
                    }
                case BasePath.FromReflection:
                    try
                    {
                        path = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath);
                        if (path[path.Length - 1] != '\\')
                            path += "\\\\";

                        exception = null;
                        return true;
                    }
                    catch (Exception e)
                    {
                        path = null;
                        exception = e;
                        return false;
                    }
                default:
                    path = null;
                    exception = new ArgumentOutOfRangeException(nameof(basePath), basePath, null);
                    return false;
            }
        }

        private static bool TryGetDirectoryParent(string basePath, int numberOfParents, out string pathParent, out Exception exception)
        {
            try
            {
                for (int i = 0; i < numberOfParents; i++)
                {
                    basePath = Path.GetDirectoryName(basePath);
                }
                pathParent = basePath;
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                exception = new Exception($"Could not get directory parent. {nameof(basePath)}:\n{basePath}", e);
                pathParent = null;
                return false;
            }
        }

        private static bool TryCreateItem(ref string path, Item item, out Exception exception)
        {
            try
            {
                switch (item)
                {
                    case Item.Directory:
                        Directory.CreateDirectory(path);
                        break;
                    case Item.File:
                        File.Create(path).Close();
                        break;
                }

                exception = null;
                return true;
            }
            catch (Exception e)
            {
                exception = new Exception($"Could not create:\n{path}", e);
                path = null;
                return false;
            }
        }
        #endregion Private methods
    }
}
