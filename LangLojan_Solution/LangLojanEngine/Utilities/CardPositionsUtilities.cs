﻿using LangLojanEngine.Domain.ValueObjects;

namespace LangLojanEngine.Utilities
{
    public static class CardPositionsUtilities
    {
        public static bool IsValid(this CardPositions cardPositions)
        {
            try
            {
                new CardPositions(cardPositions.OneStepBack, cardPositions.ThreeStepsBack);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}