﻿using System;
using System.IO;

namespace LangLojanEngine.Utilities
{
    public static class StringUtilities
    {
        public static string[] ReplaceInAll(this string[] stringArray, string oldValue, string newValue)
        {
            if (stringArray == null)
                throw new ArgumentNullException($"{nameof(stringArray)} cannot be null.");
            if (oldValue == null)
                throw new ArgumentNullException($"{nameof(oldValue)} cannot be null.");
            if (newValue == null)
                throw new ArgumentNullException($"{nameof(newValue)} cannot be null.");

            string[] strArr = new string[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
            {
                strArr[i] = stringArray[i].Replace(oldValue, newValue);
            }

            return strArr;
        }

        public static int LongestStringCount(this string[] stringArray)
        {
            if (stringArray == null)
                throw new ArgumentNullException($"{nameof(stringArray)} cannot be null.");
            int longestCount = 0;
            foreach (string s in stringArray)
            {
                longestCount = s.Length > longestCount ? s.Length : longestCount;
            }

            return longestCount;
        }
    }
}