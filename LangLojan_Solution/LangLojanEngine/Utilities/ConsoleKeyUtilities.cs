﻿using System;

namespace LangLojanEngine.Utilities
{
    public static class ConsoleKeyUtilities
    {
        public static bool IsNumberKey(this ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            if (str.Length == 2 && str[0] == 'D' && char.IsDigit(str[1]))
                return true;
            return str.Length == 7 && str.Contains("NumPad") && char.IsDigit(str[6]);
        }

        public static bool IsDigitKey(this ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            return str.Length == 2 && str[0] == 'D' && char.IsDigit(str[1]);
        }

        public static bool IsNumPadKey(this ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            return str.Length == 7 && str.Contains("NumPad") && char.IsDigit(str[6]);
        }

        public static ConsoleKey ToNumPadKey(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.D0:
                    return ConsoleKey.NumPad0;
                case ConsoleKey.D1:
                    return ConsoleKey.NumPad1;
                case ConsoleKey.D2:
                    return ConsoleKey.NumPad2;
                case ConsoleKey.D3:
                    return ConsoleKey.NumPad3;
                case ConsoleKey.D4:
                    return ConsoleKey.NumPad4;
                case ConsoleKey.D5:
                    return ConsoleKey.NumPad5;
                case ConsoleKey.D6:
                    return ConsoleKey.NumPad6;
                case ConsoleKey.D7:
                    return ConsoleKey.NumPad7;
                case ConsoleKey.D8:
                    return ConsoleKey.NumPad8;
                case ConsoleKey.D9:
                    return ConsoleKey.NumPad9;
                default:
                    return consoleKey;
            }
        }

        public static ConsoleKey ToDigitKey(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.NumPad0:
                    return ConsoleKey.D0;
                case ConsoleKey.NumPad1:
                    return ConsoleKey.D1;
                case ConsoleKey.NumPad2:
                    return ConsoleKey.D2;
                case ConsoleKey.NumPad3:
                    return ConsoleKey.D3;
                case ConsoleKey.NumPad4:
                    return ConsoleKey.D4;
                case ConsoleKey.NumPad5:
                    return ConsoleKey.D5;
                case ConsoleKey.NumPad6:
                    return ConsoleKey.D6;
                case ConsoleKey.NumPad7:
                    return ConsoleKey.D7;
                case ConsoleKey.NumPad8:
                    return ConsoleKey.D8;
                case ConsoleKey.NumPad9:
                    return ConsoleKey.D9;
                default:
                    return consoleKey;
            }
        }
    }
}