﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConsoleApplicationExtensions;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Interfaces;
using LangLojanEngine.Utilities;

namespace DebugCardCollectionCreator
{
    internal class DCCCProgram : ConsoleExtension
    {
        private static readonly Random random = new Random();
        private string[] Subdirectories => new[] { nameof(LangLojanEngine), "Resources" };

        private enum DebugItem
        {
            DebugCardCollection,
            DebugTable
        }

        private struct OutParametersContainer
        {
            public object Object { get; }
            public string FileName { get; }
            public string Message { get; }
            public Exception Exception { get; }

            public OutParametersContainer(object obj, string fileName, string message, Exception exception)
            {
                Object = obj;
                FileName = fileName;
                Message = message;
                Exception = exception;
            }
        }

        protected override void Initialize()
        {
            Print("NOTE! This program will only work if you run it inside Visual Studio!", true, ConsoleColor.DarkRed);

            AddCommand(ConsoleKey.C, $"Create debug {nameof(CardCollection)}", CreateDCC);
            AddCommand(ConsoleKey.R, $"Create debug {nameof(CardCollection)} with random cards", CreateDCCWithRandomCards);
            AddCommand(ConsoleKey.M, $"Create debug {nameof(CardCollection)} with matching cards", CreateDCCWithMatchingCards);
            AddCommand(ConsoleKey.T, $"Create debug {nameof(Table)}", CreateDebugTabel);
            AddCommand(ConsoleKey.V, "View existing debug items", ViewExistingDebugItems);
            AddCommand(ConsoleKey.Q, "Quit", Quit);
        }

        private static void Main(string[] args)
        {
            new DCCCProgram();
        }

        private void CreateDCC()
        {
            Print($"Create and save new debug {nameof(CardCollection)}.\n", false, ConsoleColor.DarkGreen);

            uint numberOfCardsInEachSuit;
            AskFor(out numberOfCardsInEachSuit, "Enter the number of cards in each suit", new UIntInputRange(1, 13, 0));
            if (numberOfCardsInEachSuit == 0)
            {
                PrintReturningToMainMenu($"Debug {nameof(CardCollection)} creation aborted.");
                return;
            }

            bool shuffle;
            AskFor(out shuffle, "Shuffle card collection?");
            CardCollection cardCollection = new CardCollection(numberOfCardsInEachSuit, shuffle);
            string fileName = GetFileName(shuffle, numberOfCardsInEachSuit);

            Print($"Successfully created a debug {nameof(CardCollection)} with {numberOfCardsInEachSuit} cards in each suit.");
            if (Confirm("Do you want to view the content?"))
            {
                PrintItemString(null, cardCollection.ToString(true));
            }
            
            OutParametersContainer outParameters;
            bool saveSuccessful = TrySaveDebugItemToXml(DebugItem.DebugCardCollection, cardCollection, fileName, out outParameters);

            PrintReturningToMainMenu(outParameters.Message, outParameters.Exception, !saveSuccessful);
        }

        private void CreateDCCWithRandomCards()
        {
            Print($"Create and save new debug {nameof(CardCollection)} with random cards.\n", false, ConsoleColor.DarkGreen);

            uint totalNumberOfCards;
            AskFor(out totalNumberOfCards, "Enter the total number of cards", new UIntInputRange(1, 52, 0));
            if (totalNumberOfCards == 0)
            {
                PrintReturningToMainMenu($"Debug {nameof(CardCollection)} creation aborted.");
                return;
            }
            List<Card> tmp = new CardCollection().ToList();
            List<Card> listOfCards = new List<Card>();
            for (int i = 0; i < totalNumberOfCards; i++)
            {
                int randomPos = random.Next(tmp.Count());
                listOfCards.Add(tmp[randomPos]);
                tmp.RemoveAt(randomPos);
            }
            CardCollection cardCollection = new CardCollection(listOfCards);
            string fileName = GetFileName(totalNumberOfCards);

            Print($"Successfully created a debug {nameof(CardCollection)} with random cards.");
            if (Confirm("Do you want to view the content?"))
            {
                PrintItemString(null, cardCollection.ToString(true));
            }

            OutParametersContainer outParameters;
            bool saveSuccessful = TrySaveDebugItemToXml(DebugItem.DebugCardCollection, cardCollection, fileName, out outParameters);

            PrintReturningToMainMenu(outParameters.Message, outParameters.Exception, !saveSuccessful);
        }

        private void CreateDCCWithMatchingCards()
        {
            Print($"Create and save new debug {nameof(CardCollection)} with matching cards.\n", false, ConsoleColor.DarkGreen);

            uint numberOfCards,
                minNumberOfCards;
            bool matchOneCardBack,
                 matchThreeCardsBack;
            AskFor(out matchOneCardBack, "Match one card back");
            AskFor(out matchThreeCardsBack, "Match three cards back");
            if (matchThreeCardsBack)
                minNumberOfCards = 4;
            else if (matchOneCardBack)
                minNumberOfCards = 2;
            else
            {
                PrintReturningToMainMenu("The last card must have at least one match.");
                return;
            }

            AskFor(out numberOfCards, "Enter number of cards", new UIntInputRange(minNumberOfCards, 8, 0));
            if (numberOfCards == 0)
            {
                PrintReturningToMainMenu($"Debug {nameof(CardCollection)} creation aborted.");
                return;
            }

            Table table = new Table(new CardCollection());
            const int maxAttempts = 10000;
            int attemptsValue = maxAttempts,
                attemptCounter = 0;
            while (attemptCounter < attemptsValue)
            {
                attemptCounter++;
                table = new Table(new CardCollection());
                while (table.CanTakeNewCard && table.LaidCards.Count < numberOfCards)
                {
                    table.TakeNewCard();
                }
                if (table.LaidCards.Count == numberOfCards)
                {
                    if (matchOneCardBack == table.IsMovableOneStepBack(table.LaidCards.Last()) &&
                        matchThreeCardsBack == table.IsMovableThreeStepsBack(table.LaidCards.Last()))
                        break;
                }

                if (attemptCounter != attemptsValue)
                    continue;

                Print($"Could not create a debug {nameof(CardCollection)} with thoose specs after {attemptsValue} attempts.", false, ConsoleColor.DarkRed);
                if (Confirm("Try again?"))
                    attemptsValue += maxAttempts;
                else
                {
                    PrintReturningToMainMenu($"Debug {nameof(CardCollection)} creation aborted.");
                    return;
                }
            }

            Print($"Successfully created a debug {nameof(CardCollection)} with thoose specs after {attemptCounter} attempts.", false, ConsoleColor.DarkGray);
            if (Confirm("Do you want to view the content?"))
            {
                for (int i = 0; i < table.LaidCards.Count; i++)
                {
                    if (i == table.LaidCards.Count - 4 && matchThreeCardsBack)
                        Print(table.LaidCards[i].ToString(), false, ConsoleColor.DarkYellow);
                    else if (i == table.LaidCards.Count - 2 && matchThreeCardsBack)
                        Print(table.LaidCards[i].ToString(), false, ConsoleColor.DarkYellow);
                    else if (i == table.LaidCards.Count - 1)
                        Print(table.LaidCards[i].ToString(), false, ConsoleColor.DarkGreen);
                    else
                        Console.WriteLine(table.LaidCards[i]);
                }
            }

            CardCollection cardCollection = new CardCollection(table.LaidCards);

            string fileName = GetFileName("Last", matchOneCardBack, matchThreeCardsBack, numberOfCards);

            OutParametersContainer outParameters;
            bool saveSuccessful = TrySaveDebugItemToXml(DebugItem.DebugCardCollection, cardCollection, fileName, out outParameters);

            PrintReturningToMainMenu(outParameters.Message, outParameters.Exception, !saveSuccessful);
        }

        private void CreateDebugTabel()
        {
            Print($"Create and save new debug {nameof(Table)}.\n", false, ConsoleColor.DarkGreen);

            uint numberOfCardsInEachSuit;
            AskFor(out numberOfCardsInEachSuit, "Create deck of cards.\nEnter the number of cards in each suit", new UIntInputRange(1, 13, 0));
            if (numberOfCardsInEachSuit == 0)
            {
                PrintReturningToMainMenu($"Debug {nameof(Table)} creation aborted.");
                return;
            }
            Table table = new Table(new CardCollection(numberOfCardsInEachSuit, true));

            OutParametersContainer outParameters;
            
            if (!TrySaveDebugItemToXml(DebugItem.DebugTable, table, $"{DebugItem.DebugTable}_{table.DeckOfCards.Count}Cards", out outParameters))
            {
                PrintReturningToMainMenu(outParameters.Message, outParameters.Exception);
                return;
            }

            Print(outParameters.Message, false);
            if (Confirm("Do you want to view the content?"))
                PrintItemString(null, table.ToString());

            PrintReturningToMainMenu();
        }
        
        private void ViewExistingDebugItems()
        {
            DebugItem? debugItem = GetDebugItemFromInput("What kind of item do you want to view?");
            if (debugItem == null)
            {
                PrintReturningToMainMenu("View selection aborted.");
                return;
            }
            bool loadAnOtherFile = true;
            while (loadAnOtherFile)
            {
                Console.Clear();
                Print($"View existing debug {debugItem.ToString().Replace("Debug", "")}'s:\n", false, ConsoleColor.DarkGreen);

                OutParametersContainer outParameters;
                if (!TryLoadDebugItemFromXml(debugItem.Value, out outParameters))
                {
                    PrintReturningToMainMenu($"Could not load: {outParameters.FileName}", outParameters.Exception);
                    return;
                }
                switch (debugItem)
                {
                    case DebugItem.DebugCardCollection:
                        PrintItemString($"Successfully loaded:{outParameters.FileName}", (outParameters.Object as CardCollection).ToString(true));
                        break;
                    case DebugItem.DebugTable:
                        PrintItemString($"Successfully loaded:{outParameters.FileName}", (outParameters.Object as Table).ToString());
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(debugItem), debugItem, null);
                }
                
                loadAnOtherFile = Confirm("View an other file?");
                if (loadAnOtherFile)
                    Console.Clear();
            }
            PrintReturningToMainMenu();
        }

        private DebugItem? GetDebugItemFromInput(string promt)
        {
            ConsoleKey answer;
            AskFor(out answer, promt, new OptionsDictionary(CreateOptionsDictionary(
                                                            new[] { ConsoleKey.C, ConsoleKey.T },
                                                            new[] { DebugItem.DebugCardCollection.ToString(), DebugItem.DebugTable.ToString() })));
            switch (answer)
            {
                case ConsoleKey.C:
                    return DebugItem.DebugCardCollection;
                case ConsoleKey.T:
                    return DebugItem.DebugTable;
                default:
                    return null;
            }
        }
        
        private bool TryLoadDebugItemFromXml(DebugItem debugItem, out OutParametersContainer outParameters)
        {
            string directoryPath;
            Exception exception;
            if (!TryGetPath.ToDirectoryInSolution(AddWithSubdirectories(debugItem),
                                                  false, out directoryPath, out exception))
            {
                outParameters = new OutParametersContainer(null, null, "Could not get directory path.", exception);
                return false;
            }

            try
            {
                string[] filePaths = Directory.GetFiles(directoryPath, "*.xml");
                if (filePaths.Length == 0)
                {
                    outParameters = new OutParametersContainer(null, null, $"Could not find any debug {debugItem}'s in:\n{directoryPath}", null);
                    return false;
                }
                Print($"Found existing debug {debugItem}'s in:\n{directoryPath + "\\"}", false, ConsoleColor.DarkGreen);
                int answer;
                const bool startOptionAtZero = false;
                AskFor(out answer, "Enter the number of the file that you want to view",
                    new OptionsStringArray(filePaths.ReplaceInAll(directoryPath + "\\", ""), startOptionAtZero),
                    new IntInputRange(1, filePaths.Length, -1));
                if (answer == -1)
                {
                    outParameters = new OutParametersContainer(null, null, "No file selected.", null);
                    return false;
                }
                answer = startOptionAtZero ? answer : answer - 1;
                string fileName = Path.GetFileName(filePaths[answer]);
                switch (debugItem)
                {
                    case DebugItem.DebugCardCollection:
                        CardCollection cardCollection;
                        if (!XmlHandler.TryLoadFrom(filePaths[answer], out cardCollection, out exception))
                        {
                            outParameters = new OutParametersContainer(null, null, $"Could not load: {fileName}", exception);
                            return false;
                        }
                        outParameters = new OutParametersContainer(cardCollection, fileName, null, null);
                        return true;
                    case DebugItem.DebugTable:
                        Table table;
                        if (!XmlHandler.TryLoadFrom(filePaths[answer], out table, out exception))
                        {
                            outParameters = new OutParametersContainer(null, null, $"Could not load: {fileName}", exception);
                            return false;
                        }
                        outParameters = new OutParametersContainer(table, fileName, null, null);
                        return true;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(debugItem), debugItem, null);
                }
            }
            catch (Exception e)
            {
                outParameters = new OutParametersContainer(null, null, $"Could not get files in:\n{directoryPath}", e);
                return false;
            }
        }

        private void PrintItemString(string prompt, string itemString)
        {
            Print(prompt, false, ConsoleColor.DarkGreen);
            Console.WriteLine("\n=====================");
            Console.WriteLine(itemString);
            Console.WriteLine("=====================\n");
        }

        private string[] AddWithSubdirectories(DebugItem debugItem)
        {
            string[] subdirectories = new string[Subdirectories.Length + 1];
            for (int i = 0; i < Subdirectories.Length; i++)
            {
                subdirectories[i] = Subdirectories[i];
            }
            subdirectories[Subdirectories.Length] = debugItem.ToString();
            return subdirectories;
        }

        private bool TrySaveDebugItemToXml(DebugItem item, IXmlItem xmlItem, string name, out OutParametersContainer outParameters)
        {
            if (xmlItem == null)
            {
                outParameters = new OutParametersContainer(null, null, "Could not save xml file.", new ArgumentNullException($"{nameof(xmlItem)} cannot be null."));
                return false;
            }

            string filePath;
            Exception pathException;
            if (TryGetPath.ToFileInSolution(name, AddWithSubdirectories(item),
                false, out filePath, out pathException))
            {
                if (!File.Exists(filePath + GetFileEnding(0)))
                    filePath += GetFileEnding(0);
                else
                {
                    if (Confirm($"Do you want to overwrite the existing file: {Path.GetFileName(filePath + GetFileEnding(0))}"))
                    {
                        uint fileCounter = 1;
                        while (File.Exists(filePath + GetFileEnding(fileCounter)) || fileCounter < 10)
                        {
                            fileCounter++;
                        }
                        filePath += GetFileEnding(fileCounter);
                    }
                }
                Exception xmlException;
                if (XmlHandler.TrySaveTo(filePath, xmlItem, out xmlException))
                {
                    string message = $"Saved xml file at:\n{filePath}" +
                                     $"Do not forget to change the properties of the file {Path.GetFileName(filePath)} to:\n" +
                                      "Build Action: 'None'\n" +
                                      "Copy to Output Directory: 'Copy if newer'";
                    outParameters = new OutParametersContainer(null, Path.GetFileName(filePath), message, null);
                    return true;
                }

                outParameters = new OutParametersContainer(null, null, $"Could not save xml file at:\n{filePath}", xmlException);
            }
            else
                outParameters = new OutParametersContainer(null, null, "Could not get file path.", pathException);

            return false;
        }

        private string GetFileName(uint numberOfCards)
        {
            StringBuilder outBuilder = new StringBuilder("DCC_");
            outBuilder.Append("Random_");
            outBuilder.Append(GetNumberOfCardsAsString(numberOfCards));
            outBuilder.Append("Cards");

            return outBuilder.ToString();
        }

        private string GetFileName(bool shuffled, uint numberOfCardsInEachSuit)
        {
            StringBuilder outBuilder = new StringBuilder("DCC_");
            outBuilder.Append(shuffled ? "Shuffled_" : "Unshuffled_");
            outBuilder.Append(GetNumberOfCardsAsString(numberOfCardsInEachSuit * 4));
            outBuilder.Append("Cards");

            return outBuilder.ToString();
        }

        private string GetFileName(string cardThatHasMatch, bool matchOne, bool matchThree, uint numberOfCards)
        {
            StringBuilder outBuilder = new StringBuilder("DCC_");
            outBuilder.Append(cardThatHasMatch);
            outBuilder.Append("Match");
            if (matchOne && matchThree)
                outBuilder.Append("1And3");
            else if (matchOne)
                outBuilder.Append(1);
            else if (matchThree)
                outBuilder.Append(3);
            outBuilder.Append("_");
            outBuilder.Append(GetNumberOfCardsAsString(numberOfCards));
            outBuilder.Append("Cards");

            return outBuilder.ToString();
        }

        private string GetNumberOfCardsAsString(uint numberOfCards)
        {
            return numberOfCards < 10 ? $"0{numberOfCards}" : numberOfCards.ToString();
        }

        private string GetFileEnding(uint fileNumber)
        {
            StringBuilder outBuilder = new StringBuilder();
            if (fileNumber != 0)
            {
                outBuilder.Append("_");
                outBuilder.Append(fileNumber);
            }
            outBuilder.Append(".xml");

            return outBuilder.ToString();
        }

        private void Quit()
        {
            if (Confirm("Do you really want to quit?"))
                EndLoop();
        }
    }
}
