﻿using System;
using System.Collections.Generic;
using System.Linq;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangLojanEngineTests.Collections
{
    [TestClass()]
    public class CardCollectionTests
    {
        private List<Card> CompleteDeckOfCards
        {
            get
            {
                List<Card> listOfCards = new List<Card>(CardCollection.MaxCapacity);
                foreach (CardSuit cardSuit in Enum.GetValues(typeof(CardSuit)))
                {
                    foreach (CardDenomination cardDenomination in Enum.GetValues(typeof(CardDenomination)))
                    {
                        listOfCards.Add(new Card(cardDenomination, cardSuit));
                    }
                }

                return listOfCards;
            }
        }

        private List<Card> PileOfCards_TwoToSeven
        {
            get
            {
                List<Card> listOfCards = new List<Card>();
                foreach (CardSuit cardSuit in Enum.GetValues(typeof(CardSuit)))
                {
                    listOfCards.Add(new Card(CardDenomination.Two, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Three, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Four, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Five, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Six, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Seven, cardSuit));
                }

                return listOfCards;
            }
        }

        private List<Card> PileOfCards_EightToJack
        {
            get
            {
                List<Card> listOfCards = new List<Card>();
                foreach (CardSuit cardSuit in Enum.GetValues(typeof(CardSuit)))
                {
                    listOfCards.Add(new Card(CardDenomination.Eight, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Nine, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Ten, cardSuit));
                    listOfCards.Add(new Card(CardDenomination.Jack, cardSuit));
                }

                return listOfCards;
            }
        }

        [TestMethod]
        public void CardCollectionTest_Create_1()
        {
            CardCollection cardCollection = new CardCollection();
            Assert.AreEqual(CardCollection.MaxCapacity, cardCollection.Count);
        }

        [TestMethod]
        public void CardCollectionTest_Create_2()
        {
            CardCollection cardCollection = new CardCollection();
            Assert.IsFalse(cardCollection.Contains(null));
        }

        [TestMethod]
        public void CardCollectionTest_Create_3()
        {
            CardCollection cardCollection = new CardCollection();
            bool isShuffled = false;
            for (int i = 0; i < cardCollection.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(CompleteDeckOfCards.ElementAt(i)))
                {
                    isShuffled = true;
                    break;
                }
            }

            Assert.IsTrue(isShuffled);
            Assert.IsTrue(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_4()
        {
            CardCollection cardCollection = new CardCollection(CompleteDeckOfCards, false);
            Assert.AreEqual(CompleteDeckOfCards.Count, cardCollection.Count);
        }

        [TestMethod]
        public void CardCollectionTest_Create_5()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, false);
            Assert.AreEqual(PileOfCards_TwoToSeven.Count, cardCollection.Count);
        }

        [TestMethod]
        public void CardCollectionTest_Create_6()
        {
            CardCollection cardCollection = new CardCollection(CompleteDeckOfCards, false);
            bool isShuffled = false;
            for (int i = 0; i < cardCollection.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(CompleteDeckOfCards.ElementAt(i)))
                {
                    isShuffled = true;
                    break;
                }
            }

            Assert.IsFalse(isShuffled);
            Assert.IsFalse(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_7()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, false);
            bool isShuffled = false;
            for (int i = 0; i < cardCollection.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(PileOfCards_TwoToSeven.ElementAt(i)))
                {
                    isShuffled = true;
                    break;
                }
            }

            Assert.IsFalse(isShuffled);
            Assert.IsFalse(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_8()
        {
            CardCollection cardCollection = new CardCollection(CompleteDeckOfCards, false);
            Assert.IsFalse(cardCollection.Contains(null));
        }

        [TestMethod]
        public void CardCollectionTest_Create_9()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, false);
            Assert.IsFalse(cardCollection.Contains(null));
        }

        [TestMethod]
        public void CardCollectionTest_Create_10()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, true);
            bool isShuffled = false;
            for (int i = 0; i < cardCollection.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(PileOfCards_TwoToSeven.ElementAt(i)))
                {
                    isShuffled = true;
                    break;
                }
            }

            Assert.IsTrue(isShuffled);
            Assert.IsTrue(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_11()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, true);
            bool isShuffled = false;
            for (int i = 0; i < cardCollection.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(PileOfCards_TwoToSeven.ElementAt(i)))
                {
                    isShuffled = true;
                    break;
                }
            }

            Assert.IsTrue(isShuffled);
            Assert.IsTrue(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_12()
        {
            CardCollection cardCollection = new CardCollection(13, false);
            Assert.AreEqual(52, cardCollection.Count);
            Assert.IsFalse(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_13()
        {
            CardCollection cardCollection = new CardCollection(13, true);
            Assert.AreEqual(52, cardCollection.Count);
            Assert.IsTrue(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_14()
        {
            CardCollection cardCollection = new CardCollection(1, false);
            Assert.AreEqual(4, cardCollection.Count);
            Assert.IsFalse(cardCollection.IsShuffled);
        }

        [TestMethod]
        public void CardCollectionTest_Create_15()
        {
            CardCollection cardCollection = new CardCollection(1, true);
            Assert.AreEqual(4, cardCollection.Count);
            Assert.IsTrue(cardCollection.IsShuffled);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CardCollectionTest_InvalidCreate_1()
        {
            new CardCollection(null, false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CardCollectionTest_InvalidCreate_2()
        {
            new CardCollection(new List<Card>(), false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CardCollectionTest_InvalidCreate_3()
        {
            List<Card> listOfCards = CompleteDeckOfCards;
            listOfCards.Add(new Card(CardDenomination.Ace, CardSuit.Clubs));
            new CardCollection(listOfCards, false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CardCollectionTest_InvalidCreate_4()
        {
            new CardCollection(0, false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CardCollectionTest_InvalidCreate_5()
        {
            new CardCollection(14, false);
        }



        [TestMethod]
        public void CardCollectionTest_TakeFirst_1()
        {
            CardCollection cardCollection = new CardCollection();
            Assert.IsNotNull(cardCollection.TakeFirst());
        }

        [TestMethod]
        public void CardCollectionTest_TakeFirst_2()
        {
            CardCollection cardCollection = new CardCollection();
            cardCollection.TakeFirst();
            Assert.AreEqual(CardCollection.MaxCapacity - 1, cardCollection.Count);
        }

        [TestMethod]
        public void CardCollectionTest_TakeFirst_3()
        {
            CardCollection cardCollection = new CardCollection();
            while (cardCollection.Count != 0)
            {
                cardCollection.TakeFirst();
            }

            Assert.IsNull(cardCollection.TakeFirst());
        }


        
        [TestMethod]
        public void CardCollectionTest_AddToTop_1()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, false);
            cardCollection.AddToTop(new Card(CardDenomination.Eight, CardSuit.Clubs));
            Assert.AreEqual(PileOfCards_TwoToSeven.Count + 1, cardCollection.Count);
        }

        [TestMethod]
        public void CardCollectionTest_AddToTop_2()
        {
            CardCollection cardCollection = new CardCollection(PileOfCards_TwoToSeven, false);
            cardCollection.AddToTop(PileOfCards_EightToJack);
            bool cardsAreInTheRightOrder = true;
            for (int i = 0; i < PileOfCards_EightToJack.Count; i++)
            {
                if (!cardCollection.ElementAt(i).Equals(PileOfCards_EightToJack.ElementAt(i)))
                {
                    cardsAreInTheRightOrder = false;
                    break;
                }
            }

            Assert.IsTrue(cardsAreInTheRightOrder);
        }
        
        

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CardCollectionTest_InvalidAddToTop_1()
        {
            CardCollection cardCollection = new CardCollection();
            Card card = null;
            cardCollection.AddToTop(card);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CardCollectionTest_InvalidAddToTop_2()
        {
            CardCollection cardCollection = new CardCollection();
            List<Card> cardList = null;
            cardCollection.AddToTop(cardList);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CardCollectionTest_InvalidAddToTop_3()
        {
            CardCollection cardCollection = new CardCollection();
            Card secondCard = cardCollection.ElementAt(1);
            cardCollection.TakeFirst();
            cardCollection.AddToTop(secondCard);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CardCollectionTest_InvalidAddToTop_4()
        {
            CardCollection cardCollection = new CardCollection();
            List<Card> cardList = new List<Card>();
            cardCollection.AddToTop(cardList);
        }

        
    }
}