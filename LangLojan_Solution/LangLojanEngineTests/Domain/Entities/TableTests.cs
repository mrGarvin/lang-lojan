﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using LangLojanEngine.Collections;
using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using LangLojanEngine.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangLojanEngineTests.Domain.Entities
{
    [TestClass]
    public class TableTests
    {
        private static string[] Subdirectories => new[] { nameof(LangLojanEngine), "Resources", "DebugCardCollections" };
        
        private static ReadOnlyCardCollection DCC_Shuffled_52Cards { get; set; }
        private static ReadOnlyCardCollection DCC_LastMatch1_03Cards { get; set; }
        private static ReadOnlyCardCollection DCC_LastMatch1And3_04Cards { get; set; }
        private static ReadOnlyCardCollection DCC_LastMatch1And3_08Cards { get; set; }
        private static ReadOnlyCardCollection ShuffledCC_52Cards { get; set; }
        private static ReadOnlyCardCollection ShuffledCC_28Cards { get; set; }
        private static ReadOnlyCardCollection ShuffledCC_12Cards { get; set; }
        private static ReadOnlyCardCollection ShuffledCC_04Cards { get; set; }

        [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            ShuffledCC_52Cards = new ReadOnlyCardCollection(new CardCollection());
            ShuffledCC_28Cards = new ReadOnlyCardCollection(new CardCollection(7, true));
            ShuffledCC_12Cards = new ReadOnlyCardCollection(new CardCollection(3, true));
            ShuffledCC_04Cards = new ReadOnlyCardCollection(new CardCollection(1, true));
            DCC_Shuffled_52Cards = new ReadOnlyCardCollection(LoadDCCFrom("DCC_Shuffled_52Cards_1.xml"));
            DCC_LastMatch1_03Cards = new ReadOnlyCardCollection(LoadDCCFrom("DCC_LastMatch1_03Cards_1.xml"));
            DCC_LastMatch1And3_04Cards = new ReadOnlyCardCollection(LoadDCCFrom("DCC_LastMatch1And3_04Cards_1.xml"));
            DCC_LastMatch1And3_08Cards = new ReadOnlyCardCollection(LoadDCCFrom("DCC_LastMatch1And3_08Cards_1.xml"));
        }

        private static CardCollection LoadDCCFrom(string fileName)
        {
            CardCollection cardCollection;
            string path;
            Exception exception;
            if (!TryGetPath.ToDirectoryInSolution(Subdirectories, false, out path, out exception))
                throw new Exception($"Could not get directory path. Directory exists: {Directory.Exists(path)}", exception);

            if (!XmlHandler.TryLoadFrom(Path.Combine(path, fileName), out cardCollection, out exception))
                throw new Exception($"Could not load '{fileName}' in:\n{path}", exception);

            return cardCollection;
        }

        [TestMethod]
        public void NewTable_CreateValidWithDebug_1()
        {
            Table table = new Table(DCC_Shuffled_52Cards.ToCardCollection());
            Assert.AreEqual(DCC_Shuffled_52Cards.Count, table.DeckOfCards.Count);
            Assert.AreEqual(0, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void NewTable_CreateValid_1()
        {
            Table table = new Table(ShuffledCC_52Cards.ToCardCollection());
            Assert.AreEqual(ShuffledCC_52Cards.Count, table.DeckOfCards.Count);
            Assert.AreEqual(0, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void NewTable_CreateValid_2()
        {
            Table table = new Table(ShuffledCC_28Cards.ToCardCollection());
            Assert.AreEqual(ShuffledCC_28Cards.Count, table.DeckOfCards.Count);
            Assert.AreEqual(0, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void NewTable_CreateValid_3()
        {
            Table table = new Table(ShuffledCC_12Cards.ToCardCollection());
            Assert.AreEqual(ShuffledCC_12Cards.Count, table.DeckOfCards.Count);
            Assert.AreEqual(0, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void NewTable_CreateValid_4()
        {
            Table table = new Table(ShuffledCC_04Cards.ToCardCollection());
            Assert.AreEqual(ShuffledCC_04Cards.Count, table.DeckOfCards.Count);
            Assert.AreEqual(0, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewTable_CreateInvalid_1()
        {
            new Table(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NewTable_CreateInvalid_2()
        {
            CardCollection cardCollection =
                new CardCollection(new List<Card> {new Card(CardDenomination.Ace, CardSuit.Clubs)}, true);
            cardCollection.TakeFirst();
            new Table(cardCollection);
        }

        [TestMethod]
        public void TakeNewCardTestWithDebug_1()
        {
            Table table = new Table(DCC_Shuffled_52Cards.ToCardCollection());
            table.TakeNewCard();
            Assert.AreEqual(DCC_Shuffled_52Cards.Count - 1, table.DeckOfCards.Count);
            Assert.AreEqual(1, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void TakeNewCardTestWithDebug_2()
        {
            Table table = new Table(DCC_Shuffled_52Cards.ToCardCollection());
            table.TakeNewCard();
            table.TakeNewCard();
            Assert.AreEqual(DCC_Shuffled_52Cards.Count - 2, table.DeckOfCards.Count);
            Assert.AreEqual(2, table.LaidCards.Count);
            if (table.CanTakeNewCard)
                Assert.IsTrue(table.CanTakeNewCard);
            else
                Assert.IsFalse(table.CanTakeNewCard);
            Assert.AreEqual(table.CanTakeNewCard ? 0 : 1, table.MovableCards.Count);
        }

        [TestMethod]
        public void TakeNewCardTestWithDebug_3()
        {
            Table table = new Table(DCC_LastMatch1_03Cards.ToCardCollection());
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    table.TakeNewCard();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
            Assert.AreEqual(DCC_LastMatch1_03Cards.Count - 3, table.DeckOfCards.Count);
            Assert.AreEqual(3, table.LaidCards.Count);
            Assert.AreEqual(1, table.MovableCards.Count);
            Assert.IsFalse(table.CanTakeNewCard);
        }

        [TestMethod]
        public void TakeNewCardTestWithDebug_4()
        {
            Table table = new Table(DCC_LastMatch1And3_04Cards.ToCardCollection());
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    table.TakeNewCard();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
            Assert.AreEqual(DCC_LastMatch1And3_04Cards.Count - 4, table.DeckOfCards.Count);
            Assert.AreEqual(4, table.LaidCards.Count);
            Assert.AreEqual(1, table.MovableCards.Count);
            Assert.IsFalse(table.CanTakeNewCard);
        }

        [TestMethod]
        public void TakeNewCardTestWithDebug_5()
        {
            Table table = new Table(DCC_LastMatch1And3_08Cards.ToCardCollection());
            try
            {
                for (int i = 0; i < 8; i++)
                {
                    table.TakeNewCard();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
            Assert.AreEqual(DCC_LastMatch1And3_08Cards.Count - 8, table.DeckOfCards.Count);
            Assert.AreEqual(8, table.LaidCards.Count);
            Assert.AreEqual(1, table.MovableCards.Count);
            Assert.IsFalse(table.CanTakeNewCard);
        }

        [TestMethod]
        public void TakeNewCardTest_1()
        {
            Table table = new Table(ShuffledCC_52Cards.ToCardCollection());
            table.TakeNewCard();
            Assert.AreEqual(ShuffledCC_52Cards.Count - 1, table.DeckOfCards.Count);
            Assert.AreEqual(1, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
            Assert.IsTrue(table.CanTakeNewCard);
        }

        [TestMethod]
        public void TakeNewCardTest_2()
        {
            Table table = new Table(ShuffledCC_52Cards.ToCardCollection());
            table.TakeNewCard();
            table.TakeNewCard();
            Assert.AreEqual(ShuffledCC_52Cards.Count - 2, table.DeckOfCards.Count);
            Assert.AreEqual(2, table.LaidCards.Count);
            if (table.CanTakeNewCard)
                Assert.IsTrue(table.CanTakeNewCard);
            else
                Assert.IsFalse(table.CanTakeNewCard);
            Assert.AreEqual(table.CanTakeNewCard ? 0 : 1, table.MovableCards.Count);
        }

        [TestMethod]
        public void MoveOneStepBackTestWithDebug_1()
        {
            Table table = new Table(DCC_LastMatch1_03Cards.ToCardCollection());
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    table.TakeNewCard();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
            Assert.IsFalse(table.CanTakeNewCard);
            Assert.IsTrue(table.IsMovableOneStepBack(table.MovableCards[0].Card));
            Assert.AreEqual(DCC_LastMatch1_03Cards.Count - 3, table.DeckOfCards.Count);
            Assert.AreEqual(3, table.LaidCards.Count);
            Assert.AreEqual(1, table.MovableCards.Count);
            table.MoveOneStepBack(table.MovableCards[0].Card);
            Assert.IsTrue(table.CanTakeNewCard);
            Assert.AreEqual(DCC_LastMatch1_03Cards.Count - 3, table.DeckOfCards.Count);
            Assert.AreEqual(2, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
        }

        [TestMethod]
        public void MoveOneStepBackTestWithDebug_2()
        {
            Table table = new Table(DCC_LastMatch1And3_04Cards.ToCardCollection());
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    table.TakeNewCard();
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
            Assert.IsFalse(table.CanTakeNewCard);
            Assert.IsTrue(table.IsMovableOneStepBack(table.MovableCards[0].Card));
            Assert.AreEqual(DCC_LastMatch1And3_04Cards.Count - 4, table.DeckOfCards.Count);
            Assert.AreEqual(4, table.LaidCards.Count);
            Assert.AreEqual(1, table.MovableCards.Count);
            Assert.IsNotNull(table.MovableCards[0].CardPositions.OneStepBack);
            Assert.IsNotNull(table.MovableCards[0].CardPositions.ThreeStepsBack);
            table.MoveOneStepBack(table.MovableCards[0].Card);
            Assert.IsTrue(table.CanTakeNewCard);
            Assert.AreEqual(DCC_LastMatch1And3_04Cards.Count - 4, table.DeckOfCards.Count);
            Assert.AreEqual(3, table.LaidCards.Count);
            Assert.AreEqual(0, table.MovableCards.Count);
        }
    }
}