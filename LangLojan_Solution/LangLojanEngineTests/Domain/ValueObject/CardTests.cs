﻿using LangLojanEngine.Domain.Entities;
using LangLojanEngine.Domain.Enums;
using LangLojanEngine.Domain.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LangLojanEngineTests.Domain.ValueObject
{
    [TestClass]
    public class CardTests
    {
        [TestMethod]
        public void CardTest()
        {
            CardDenomination denomination = CardDenomination.Ace;
            CardSuit suit = CardSuit.Hearts;
            Card card = new Card(denomination, suit);
            Assert.AreEqual(denomination, card.Denomination);
            Assert.AreEqual(suit, card.Suit);
        }

        [TestMethod]
        public void CardTest_Equals()
        {
            Card card = new Card(CardDenomination.King, CardSuit.Spades);
            Assert.IsTrue(card.Equals(card));
        }




        [TestMethod]
        public void CardTests_NotEquals_1()
        {
            Card card1 = new Card(CardDenomination.Ace, CardSuit.Clubs);
            Card card2 = new Card(CardDenomination.Eight, CardSuit.Diamonds);
            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void CardTests_NotEquals_2()
        {
            Card card1 = new Card(CardDenomination.Eight, CardSuit.Clubs);
            Card card2 = new Card(CardDenomination.Eight, CardSuit.Diamonds);
            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void CardTests_NotEquals_3()
        {
            Card card1 = new Card(CardDenomination.Ace, CardSuit.Clubs);
            Card card2 = new Card(CardDenomination.Eight, CardSuit.Clubs);
            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void CardTests_NotEquals_4()
        {
            Card card1 = new Card(CardDenomination.Ace, CardSuit.Clubs);
            Assert.IsFalse(card1.Equals(null));
        }

        [TestMethod]
        public void CardTests_NotEquals_5()
        {
            Card card1 = new Card(CardDenomination.Ace, CardSuit.Clubs);
            Assert.IsFalse(card1.Equals(3));
        }
    }
}